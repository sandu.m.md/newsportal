<?php
	session_start();
	date_default_timezone_set("Europe/Chisinau");
	# parrent directory
	$baseDir = __DIR__;

	if ($_GET["url"] == "favicon.ico") {
		exit();
	}

	# time init
	$timeStart = microtime(true);

	# process request
	$request = explode("/", $_GET["url"]);

	# additional params
	$GLOBALS["params"] = array_slice($request, 2);
	
	# init application
	App::init();

	# check guest
	App::checkSess();

	# check user session
	App::$user->checkSession();

	if (isset($_COOKIE["theme"])) {
		App::$theme = App::$conf->themes->{$_COOKIE["theme"]};
	} else {
		App::$theme = App::$conf->themes->{"dark"};
		setcookie("theme", "dark", time()+60*60*24*365);
	}

	if (isset($_SESSION["guestId"])) {
		// App::$log->error($_SESSION["guestId"]);
	} else {
		// App::$log->error("Epta");
	}

	# routing
	if (isset($request[0])) {
		# check requested controller
		$className = ucfirst($request[0]) . "Controller";

		if (class_exists($className)) {
			$page = new $className();

			if (isset($request[1])) {
				$methodName = $request[1];

				if (method_exists($page, $methodName)) {
					$page->{$methodName}();	
				} else {
					echo '<h1><center>Not found</center></h1><h3><center><button onclick="window.location.replace(\'/home\');">Go back</button></center></h3>';
					exit();
					# default method
					$page->index();
				}
			} else {
				// echo '<h1><center>Not found</center></h1>';
				// exit();
				# default method
				$page->index();
			}			
		} else {
			// echo '<h1><center>Not found</center></h1><h3><center><button onclick="window.location.replace(\'/home\');">Go back</button></center></h3>';
			// exit();
			# default controller
			// App::redirect("/home");
			App::redirect("/quiz");
			// $page = new DefaultController();
			// $page->index();	
		}
	} else {
		# default controller
		// App::redirect("/home");
		App::redirect("/quiz");
		// $page = new DefaultController();
		// $page->index();
	}

	function __autoload($className) {
		if (preg_match('/^\w*Controller$/', $className)) {
			$file = "protected/controllers/$className.php";

			if (file_exists($file)) {
				require_once($file);	
			}
		} elseif (preg_match('/^\w*Model$/', $className)) {
			$file = "protected/models/$className.php"; 
			
			if (file_exists($file)) {
				require_once($file);	
			}
		} elseif (preg_match('/^\w*View$/', $className)) {
			$file = "protected/views/$className.php";
			
			if (file_exists($file)) {
				require_once($file);	
			}
		} elseif (preg_match('/^\w*Component$/', $className)) {
			$file = "protected/components/$className.php";

			if (file_exists($file)) {
				require_once($file);
			}
		} elseif ($className == "Conf") {
			$file = "protected/config/Conf.php";

			if (file_exists($file)) {
				require_once($file);
			}
		} elseif ($className == "Config") {
			$file = "protected/config/Config.php";

			if (file_exists($file)) {
				require_once($file);
			}
		} elseif($className == "DbConf") {
			$file = "protected/config/DbConf.php";

			if (file_exists($file)) {
				require_once($file);
			}
		} else {
			$file = "protected/components/$className.php";

			if (file_exists($file)) {
				require_once($file);
			}
		}
	}

	if (App::$user->isLoggedIn()) {
		if ($_GET["url"] != "favicon.ico") App::$log->request(microtime(true) - $timeStart, App::$user->username, $_GET["url"], App::$respCode);
	} else {
		if ($_GET["url"] != "favicon.ico") App::$log->request(microtime(true) - $timeStart, App::$guestId, $_GET["url"], App::$respCode);
	}


?>