<?php

class ApiController
{
    public function uploadImg()
    {
        if (App::isPostRequest()) {
            if (App::isActive()) {
                $ext = end(explode(".", $_FILES["file"]["name"]));

                $target_file = "res/images/" . date("Y_m_d_H_i_s") . "_" . rand(100000, 999999) . "." . $ext;
    
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    echo App::getJsonResponse(true, array("filename" => "/". $target_file, "img_type" => $_POST["img-type"]));
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Cannot upload file"));
                }
            } else {
                echo App::getJsonResponse(false, json_encode(array("message"=>"Not connected!")));
            }
        } else {
            App::redirect("/home");
        }
    }

    public function subscribe()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["email"])) {
                if (!SubscribersModel::exists($_POST["email"])) {
                    if (SubscribersModel::insert(str_replace(array("'", "@"), array("\'", "\@"), $_POST["email"]), App::getIp())) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse();
                    }
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    public function uploadImage()
    {
        if (App::isPostRequest()) {
            if (App::$user->isLoggedIn()) {
                $ext = end(explode(".", $_FILES["file"]["name"]));

                $target_file = "res/images/" . date("Y_m_d_H_i_s") . "_" . rand(100000, 999999) . "." . $ext;
    
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                    echo App::getJsonResponse(true, array("filename" => "/". $target_file));
                } else {
                    echo App::getJsonResponse(false, false, array("message" => "Cannot upload file"));
                }
            } else {
                echo App::getJsonResponse(false);
            }
        } else {
            echo App::getJsonResponse();
        }
    }
}


?>