<?php  
class HelpController
{

	public function index()
	{
		Loader::load("UserProblems");
	}

	public function catchProblem()
	{
		if (isset($_POST["text"]) && isset($_POST["email"])) {
			HelpModel::insertProblem($_POST["text"], $_POST["email"]);
		} 

		App::redirect("/help/feedback");
	}

	public function feedback()
    {
        Loader::load("Feedback");
    }

}  
?>