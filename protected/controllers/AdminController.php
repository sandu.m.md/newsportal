<?php 

class AdminController
{
    public function index()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            Loader::load("AdminMain");
        } else {
            App::redirect("/admin/login");
        }
    }

    public function live()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            Loader::load("AdminLive");
        } else {
            App::redirect("/admin/login");
        }
    }

    public function login()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            App::redirect("/admin");
        } else {
            Loader::load("AdminLogin");
        }
    }

    public function logout()
    {
        if (App::isActive()) {
            session_unset();
            App::redirect("/home");
        } else {
            App::redirect("/admin/login");
        }
    }

    public function auth()
    {
        if ($_POST["username"] == "sandu" && $_POST["password"] == "sandu") {
            $_SESSION["username"] = $_POST["username"];
            App::$active = true;
            App::redirect("/admin");
        }
    }

    public function savePost()
    {
        if (App::isPostRequest() && App::isActive() && isset($_POST["title"]) && isset($_POST["cover"]) && isset($_POST["body"])) {
            if (isset($GLOBALS["params"][0]) || (isset($_POST["postId"]) && $_POST["postId"] != 0)) {
                $postId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : $_POST["postId"];

                App::$log->error($_POST["body"]);

                if (NewsModel::update($postId, $_POST["title"], $_POST["cover"], $_POST["body"])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                if ($postId = NewsModel::insert($_POST["title"], $_POST["cover"], $_POST["body"], 0)) {
                    echo App::getJsonResponse(true, array("postId" => $postId));
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }
}

?>