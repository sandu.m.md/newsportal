<?php 

class QuizController
{
    public function index()
    {
        Loader::load("QuizAll", array(
            "top" => QuizModel::getTop(),
            "recommended" => QuizModel::getRecommended(),
            "newest" => QuizModel::getNewest()
        ));
    }
    
    public function view()
    {
        $quizId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        if (($quiz = QuizModel::getActiveById($quizId)) && ($questions = QuizModel::getActiveQuestions($quizId))) {
            Loader::load("Quiz", array(
                "quiz" => $quiz, 
                "q" => $questions,
                "recommended" => QuizModel::getRecommended(),
                "newest" => QuizModel::getNewest(),
                "top" => QuizModel::getTop()
            ));
        } else {
            if (true || App::$user->isManager()) {
                if (($quiz = QuizModel::getById($quizId)) && ($questions = QuizModel::getActiveQuestions($quizId))) {
                    Loader::load("Quiz", array(
                        "quiz" => $quiz, 
                        "q" => $questions,
                        "recommended" => QuizModel::getRecommended(),
                        "newest" => QuizModel::getNewest(),
                        "top" => QuizModel::getTop()
                    ));
                } else {
                    echo 'Ceva a mers incorect! <a href="/user/home">Home</a>';
                }
            } else {
                App::redirect("/quiz");
            }
        }

        QuizModel::view($quizId);
    }

    // post
    public function getStatistics()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            $players = QuizModel::players();
            $stats = QuizModel::stats();
            echo App::getJsonResponse(true, array("players" => $players, "stats" => $stats));
        }
    }

    // post
    public function getSessions()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            $sessions = QuizModel::sess();
            echo App::getJsonResponse(true, array("sessions" => $sessions));
        }
    }

    // post
    public function start()
    {
        $quizId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        if ($id = QuizModel::start($quizId)) {
            echo App::getJsonResponse(true, array("id" => $id));
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function pass()
    {
        $quizId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        if (QuizModel::pass($_POST["sessionId"], $quizId, $_POST["correct_answers"], $_POST["result"])) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function answer()
    {
        $qId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

        if (QuizModel::answer($_POST["sessionId"],$qId, $_POST["answer"], $_POST["duration"])) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function disable()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($GLOBALS["params"][0])) {
                if (QuizModel::disable($GLOBALS["params"][0])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function enable()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($GLOBALS["params"][0])) {
                if (QuizModel::enable($GLOBALS["params"][0])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function disableQuestion()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($GLOBALS["params"][0])) {
                if (QuizModel::disableQuestion($GLOBALS["params"][0])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function enableQuestion()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($GLOBALS["params"][0])) {
                if (QuizModel::enableQuestion($GLOBALS["params"][0])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    public function edit()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            $quizId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

            if ($quizId == 0) {
                Loader::load("QuizEdit");
            } else {
                if ($quiz = QuizModel::getById($quizId)) {
                    if ($questions = QuizModel::getQuestions($quizId)) {
                        if ($ratingScale = QuizModel::getRatingScale($quiz["rating_scale_id"])) {
                            Loader::load("QuizEdit", array("quiz" => $quiz, "q" => $questions, "ratingScale" => $ratingScale));
                        } else {
                            Loader::load("QuizEdit", array("quiz" => $quiz, "q" => $questions));
                        }
                    } else {
                        if ($ratingScale = QuizModel::getRatingScale($quiz["rating_scale_id"])) {
                            Loader::load("QuizEdit", array("quiz" => $quiz, "ratingScale" => $ratingScale));
                        } else {
                            Loader::load("QuizEdit", array("quiz" => $quiz));
                        }
                    }
                } else {
                    Loader::load("QuizEdit");
                }
            }
        } else {
            App::redirect("/admin/login");
        }
    }

    public function saveQuiz()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($GLOBALS["params"][0])) {
                $quizId = $GLOBALS["params"][0];

                if (QuizModel::update($quizId, $_POST["title"], $_POST["cover"])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                if ($quizId = QuizModel::insert($_POST["title"], $_POST["cover"])) {
                    echo App::getJsonResponse(true, array("id" => $quizId));
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    public function editQuestion()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            if (isset($_POST["modal-quiz-id"]) && isset($_POST["modal-qid"]) && isset($_POST["modal-question"]) && isset($_POST["modal-answer-1"]) && isset($_POST["modal-answer-2"]) && isset($_POST["modal-answer-3"]) && isset($_POST["modal-correct"])) {
                if ($_POST["modal-qid"] == 0) {
                    if (QuizModel::insertQuestion($_POST["modal-quiz-id"], $_POST["modal-question"], $_POST["modal-answer-1"], $_POST["modal-answer-2"], $_POST["modal-answer-3"], $_POST["modal-correct"], App::$user->id)) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse();
                    }
                } else {
                    if (QuizModel::updateQuestion($_POST["modal-qid"], $_POST["modal-question"], $_POST["modal-answer-1"], $_POST["modal-answer-2"], $_POST["modal-answer-3"], $_POST["modal-correct"])) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse();
                    }
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            App::redirect("/admin/login");
        }
    }

    public function removeQuestion()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            $qid = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

            if (QuizModel::removeQuestion($qid)) {
                echo App::getJsonResponse(true);
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    public function remove()
    {
        if (App::$user->isLoggedIn() && App::$user->isAdmin()) {
            $quizId = isset($GLOBALS["params"][0]) ? $GLOBALS["params"][0] : 0;

            if (QuizModel::remove($quizId)) {
                echo App::getJsonResponse(true);
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }
}



?>