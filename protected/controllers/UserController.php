<?php 

class UserController
{
    public function index()
    {
        App::redirect("/user/home");
    }

    public function login()
    {
        if (App::$user->isLoggedIn()) {
            App::redirect("/user/home");
        } else {
            Loader::load("UserLogin");
        }
    }

    public function home()
    {
        if (App::$user->isLoggedIn()) {
            Loader::load("UserHome", array("recommended" => App::$user->recommendedQuizzes()));
        } else {
            App::redirect("/user/login");
        }
    }

    public function quizzes()
    {
        if (App::$user->isLoggedIn() && App::$user->isManager()) {
            if ($quizzes = App::$user->quizzes()) {
                Loader::load("UserQuizzes", array("quizzes" => $quizzes));
            } else {
                Loader::load("UserQuizzes", array("quizzes" => array()));
            }
        } else {
            App::redirect("/user/login");
        }
    }

    public function register()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["username"]) && isset($_POST["name"]) && isset($_POST["password"])) {
                if ($userId = UsersModel::register($_POST["username"], $_POST["name"], $_POST["password"], App::$guestId)) {
                    if ($user = UsersModel::getById($userId)) {
                        if (App::$user->login($user["username"], $_POST["password"])) {
                            UsersModel::sendNotification(8, App::$user->id, "Bun venit!", "Salut! Îți mulțumim că te-ai alăturat site-ului nostru! Îți urăm un joc plăcut!");
                            echo App::getJsonResponse(true);
                        } else {
                            echo App::getJsonResponse(false, false, array("message" => "Eroare la login"));
                        }
                    } else {
                        echo App::getJsonResponse(false, false, array("message" => "Eroare la get by id"));
                    }
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            if (App::$user->isLoggedIn()) {
                App::redirect("/user/home");
            } else {
                Loader::load("UserRegister");
            }
        }
    }

    public function results()
    {
        if (App::$user->isLoggedIn()) {
            Loader::load("UserResults", array("results" => App::$user->results()));
        } else {
            App::redirect("/user/login");
        }
    }

    public function notifications()
    {
        if (App::$user->isLoggedIn()) {
            Loader::load("UserNotifications", array("notifications" => App::$user->notifications()));
        } else {
            App::redirect("/user/login");
        }
    }

    public function top()
    {
        Loader::load("UserTop", array("top" => UsersModel::getRatingTop()));
    }

    // post
    public function sendNotification()
    {
        if (!App::isPostRequest()) {
            echo App::getJsonResponse();
            return;
        }

        if (!App::$user->isLoggedIn()) {
            echo App::getJsonResponse();
            return;
        }

        if (!isset($GLOBALS["params"][0]) || !isset($_POST["subject"]) || !isset($_POST["text"])) {
            echo App::getJsonResponse();
            return;
        }

        $toUserId = (int)$GLOBALS["params"][0];
        $subject = $_POST["subject"];
        $text = $_POST["text"];

        if (App::$user->sendNotification($toUserId, $subject, $text)) {
            echo App::getJsonResponse(true);
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function seenNotification()
    {
        if (App::isPostRequest() && isset($GLOBALS["params"][0])) {
            if (App::$user->isLoggedIn()) {
                App::$user->seenNotification((int)$GLOBALS["params"][0]);
                echo App::getJsonResponse(true);
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function auth()
    {
        if (App::isPostRequest()) {
            if (isset($_POST["username"]) && isset($_POST["password"])) {
                if (UsersModel::getByUsername($_POST["username"])) {
                    if (App::$user->login($_POST["username"], $_POST["password"])) {
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse(false, array("error"=>"Parolă greșită"));
                    }
                } else {
                    echo App::getJsonResponse(false, array("error"=>"Utilizător inexistent"));
                }
            } else {
                echo App::getJsonResponse();
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function logout()
    {
        if (App::$user->isLoggedIn()) {
            session_destroy();
            App::redirect("/user/login");
        } else {
            App::redirect("/user/home");
        }
    }

    public function quizEdit()
    {
        if (App::$user->isLoggedIn() && App::$user->isManager()) {
            if (isset($GLOBALS["params"][0]) && App::$user->hasAccessToQuiz($GLOBALS["params"][0])) {
                if ($quiz = QuizModel::getById($GLOBALS["params"][0])) {
                    if ($questions = QuizModel::getQuestions($GLOBALS["params"][0])) {
                        Loader::load("UserQuizEdit", array("quiz" => $quiz, "questions" => $questions));        
                    } else {
                        Loader::load("UserQuizEdit", array("quiz" => $quiz, "questions" => array()));        
                    }                    
                } else {
                    Loader::load("UserQuizEdit");
                }
            } else {
                Loader::load("UserQuizEdit");
            }
        } else {
            App::redirect("/user/login");
        }
    }

    // post
    public function saveQuiz()
    {
        if (App::$user->isLoggedIn() && App::$user->isManager()) {
            if (isset($GLOBALS["params"][0]) && App::$user->hasAccessToQuiz($GLOBALS["params"][0])) {
                $quizId = $GLOBALS["params"][0];

                if (QuizModel::update($quizId, $_POST["title"], $_POST["cover"])) {
                    echo App::getJsonResponse(true);
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                if ($quizId = QuizModel::insert(App::$user->id, $_POST["title"], $_POST["cover"])) {
                    echo App::getJsonResponse(true, array("id" => $quizId));
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }

    // post
    public function saveQuestion()
    {
        if (App::$user->isLoggedIn()) {
            if (App::$user->isManager()) {
                if (isset($_POST["quiz-id"]) && isset($_POST["question-id"]) && isset($_POST["question"]) && isset($_POST["answer-1"]) && isset($_POST["answer-2"]) && isset($_POST["answer-3"]) && isset($_POST["correct-answer"])) {
                    if ($_POST["question-id"] == 0) {
                        if (QuizModel::insertQuestion($_POST["quiz-id"], $_POST["question"], $_POST["answer-1"], $_POST["answer-2"], $_POST["answer-3"], $_POST["correct-answer"], App::$user->id)) {
                            echo App::getJsonResponse(true);
                        } else {
                            echo App::getJsonResponse();
                        }
                    } else {
                        if (App::$user->hasAccessToQuiz($_POST["quiz-id"])) {
                            if (QuizModel::updateQuestion($_POST["question-id"], $_POST["question"], $_POST["answer-1"], $_POST["answer-2"], $_POST["answer-3"], $_POST["correct-answer"])) {
                                echo App::getJsonResponse(true);
                            } else {
                                echo App::getJsonResponse();
                            }
                        } else {
                            echo App::getJsonResponse();
                        }
                    }
                } else {
                    echo App::getJsonResponse();
                }
            } else {
                if (isset($_POST["user-id"]) && isset($_POST["question-id"]) && isset($_POST["question"]) && isset($_POST["answer-1"]) && isset($_POST["answer-2"]) && isset($_POST["answer-3"]) && isset($_POST["correct-answer"])) {
                    if (QuizModel::insertQuestion(0, $_POST["question"], $_POST["answer-1"], $_POST["answer-2"], $_POST["answer-3"], $_POST["correct-answer"], $_POST["user-id"])) {
                        UsersModel::sendNotification(8, App::$user->id, "Ați sugerat o întrebare!", "Salut! Îți mulțumim pentru contribuția ta la pagina noastră. Vei fi înștiințat îndată ce întrebarea ta va fi aprobată.");
                        echo App::getJsonResponse(true);
                    } else {
                        echo App::getJsonResponse();
                    }
                } else {
                    echo App::getJsonResponse();
                }
            }
        } else {
            echo App::getJsonResponse();
        }
    }
}

?>