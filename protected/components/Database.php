<?php

class Database
{
    private $link;

    function __construct($db)
    {
        if (!($this->link = mysqli_connect($db->host, $db->user, $db->pass, $db->dbname))) {
            App::$log->error(mysqli_connect_error());
        }
    }

    public function api_install($sql)
    {
        mysqli_begin_transaction($this->link);

        if ($this->query($sql)) {
            mysqli_commit($this->link);
            return true;
        } else {
            mysqli_rollback($this->link);
            return false;
        }

    }

    public function query($sql)
    {
        if (!($result = mysqli_query($this->link, $sql))) {
            App::$log->error(mysqli_error($this->link));
            App::$log->error($sql);
            return false;
        }

        if ($result === true) {
            return true;
        }

        if (mysqli_num_rows($result) == 1) {
            $service = mysqli_fetch_assoc($result);
            return array($service["id"] => $service);
        } elseif (mysqli_num_rows($result) > 1) {
            while($row = mysqli_fetch_assoc($result)) {
                $arr[] = $row;
            }

            return $arr;
        }

        return true;
    }

    public function insertedId()
    {
        return mysqli_insert_id($this->link);
    }
}

?>