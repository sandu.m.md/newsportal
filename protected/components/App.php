<?php

class App
{
    // Main modules
    /** @var User */
    public static $user;

    /** @var Config */
    public static $conf;
    
    /** @var Logger */
    public static $log;
    
    /** @var Database */
    public static $db;

    public static $theme = "dark";
    public static $active = false;
    public static $guestId = 0;
    public static $respCode = 200;

    public static function init()
    {
        self::$conf = new Config();
        self::$log = new Logger(self::$conf->logger);
        self::$db = new Database(self::$conf->db);
        self::$user = new User();
    }

    public static function isActive()
    {
        return self::$active;
    }

    public static function checkSess()
    {
        if (isset($_SESSION["username"])) {
            self::$active = true;
            self::$guestId = $_SESSION["guestId"];
            return true;
        }

        if (isset($_SESSION["guestId"]) && ($guest = GuestsModel::getById($_SESSION["guestId"]))) {
            GuestsModel::update($_SESSION["guestId"]);
            self::$guestId = $_SESSION["guestId"];
            $_SESSION["guestId"] = $guest["id"];
            return true;
        }

        if (isset($_COOKIE["guest"]) && ($guest = GuestsModel::getById($_COOKIE["guest"]))) {
            GuestsModel::update($_COOKIE["guest"]);
            self::$guestId = $_COOKIE["guest"];
            $_SESSION["guestId"] = $_COOKIE["guest"];
            return true;
        }

        if ($id = GuestsModel::insert(App::getIp())) {
            $_SESSION["guestId"] = $id;
            self::$guestId = $id;
            return true;
        }
    }

    public static function redirect($url) 
    {
        echo "<script>window.location.replace(\"$url\");</script>";
    }
    
    public static function cLog($text)
    {
        echo "<script>console.log('$text');</script>";
    }

    public static function now()
    {
        return date("Y-m-d H:i:s");
    }

    public static function download($file)
    {
        if (file_exists($file)) {
            $baseFile = end(explode("/", $file));
            ob_start();
            ob_flush();
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $baseFile . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            if (readfile($file)) {
                unlink($file);
            }
            
            return true;
        } 

        return false;
    }

    public static function includeJs($file)
    {
        // return "<script>" . file_get_contents($file) . "</script>";
        echo "<script src=\"$file\"></script>";
        // echo "<script type=\"text/javascript\" src=\"http://wordtemplates/$file\"></script>\n";
        // echo "<script type=\"text/javascript\" src=\"http://".$_SERVER['HTTP_HOST']."/$file\"></script>\n";
    }

    public static function includeCss($file)
    {
        echo "<style>" . file_get_contents($file) . "</style>";
        // echo "<link href=\"$file\">";
        // echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://wordtemplates/$file\" />\n";
        // echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER['HTTP_HOST']."/$file\" />\n";
    }

    public static function getIp() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function getJsonResponse($ok = false, $body = false, $info = false)
    {
        return '{"ok":' . json_encode($ok) . ', "body": ' . json_encode($body) . ', "info":' . json_encode($info) . '}';
    }

    public static function isPostRequest()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public static function getLocation()
    {
        return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    public static function getHost()
    {
        return "http://178.168.16.9:8880";
        return "http://$_SERVER[HTTP_HOST]";
    }
}

?>