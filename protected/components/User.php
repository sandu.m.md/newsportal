<?php

class User
{
    private $isLoggedIn = false;
    private $type = -1;

    public function login($username, $password)
    {
        if ($user = UsersModel::getByUsername($username)) {
            if ($user["password"] == md5($password)) {

                $this->id = $user["id"];
                $this->username = $user["username"];
                $this->nick = $user["nick"];
                $this->type = $user["type"];
                $this->email = $user["email"];

                $this->isLoggedIn = true;
                
                if (!($sessionId = UsersModel::startSession($user["id"], App::getIp()))) {
                    App::$log->error(json_encode($sessionId));
                    return false;
                }
                
                $this->lastAccess = App::now();
                $this->sessionId = $sessionId;
                $_SESSION["sessionId"] = $sessionId;

                $_SESSION["guestId"] = $user["guest_id"];
                App::$guestId = $user["guest_id"];
                $this->guest_id = $user["guest_id"];

                UsersModel::updateLastLogin($user["id"]);

                return true;
            } 

            return false;
        }

        return false;
    }

    public function checkSession()
    {
        if (!isset($_SESSION["sessionId"])) {
            return false;
        }

        if (!($session = UsersModel::getSessionById($_SESSION["sessionId"]))) {
            return false;
        }
        
        if (!($user = UsersModel::getById($session["user_id"]))) {
            return false;
        }

        $dt = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . App::$conf->session->duration));

        // App::$log->error($dt.' | '.$session["datetime_last_access"]);

        // if(false)
        if ($dt > $session["datetime_last_access"]) {
            // App::$log->error($session);
            // App::$log->error($dt . " | " . date("Y-m-d H:i:s"));
            // App::$log->error($dt > $session["datetime_last_access"]);
            // App::$log->error($user);
            // App::$log->error(4);
            // session_destroy();
            return false;
        }

        if (!UsersModel::updateSesssion($session["id"])) {
            App::$log->error("Failed to update session. id => $session[id]");
        }
               
        if (isset($_SESSION["guestId"]) && $_SESSION["guestId"] != $user["guest_id"]) {
            $_SESSION["guestId"] = $user["guestId"];
            App::$guestId = $user["guest_id"];
        }

        if (!isset($_SESSION["guestId"])) {
            $_SESSION["guestId"] = $user["guestId"];
            App::$guestId = $user["guest_id"];
        }

        $this->id = $user["id"];
        $this->username = $user["username"];
        $this->nick = $user["nick"];
        $this->type = $user["type"];
        $this->email = $user["email"];
        $this->guest_id = $user["guest_id"];

        $this->isLoggedIn = true;
        
        $this->lastAccess = App::now();
        $this->sessionId = $session["id"];
        $_SESSION["sessionId"] = $session["id"];

        return true;
    }

    public function rating()
    {
        if (isset($this->rating) && isset($this->totalRatings) && isset($this->passedTests) && isset($this->percentage)) {
            // return array("rating"=>$this->rating,"total"=>$this->totalRatings,"passedTests"=>$this->passedTests,"percentage"=>$this->percentage);
            return $this->rating;
        }

        if ($result = UsersModel::getRating($this->id)) {

            if ($result["ok"] == "1") {
                $this->rating = $result;
            } else {
                $this->rating = array(
                    "rating" => "-",
                    "passed_count" => "0",
                    "percentage" => "0"
                );
            }

            

            // $this->rating = ($result["ok"] == "1" ? $result["rating"] : "-");
            // $this->totalRatings = $result["total"];
            // $this->passedTests = $result["passed_count"];
            // $this->percentage = $result["percentage"];
            // App::$log->error(json_encode($result));
            // return array("rating"=>$this->rating,"total"=>$this->totalRatings,"passedTests"=>$this->passedTests,"percentage"=>$this->percentage);
            return $this->rating;
        }

        return array("rating"=>"","total"=>0);
    }

    public function recommendedQuizzes()
    {
        return UsersModel::getRecommendedQuizzes($this->id);
    }

    public function isLoggedIn()
    {
        return $this->isLoggedIn;
    }

    public function isAdmin()
    {
        return $this->type == 1;
    }

    public function isManager()
    {
        return $this->type >= 1;
    }

    public function hasAccessToQuiz($quizId)
    {
        return QuizModel::isAllowedToUser($quizId, $this->id);
    }

    public function quizzes()
    {
        return UsersModel::getQuizzes($this->id);
    }

    public function hasNotifications()
    {
        if (isset($this->unreadNotificationsCount)) {
            return $this->unreadNotificationsCount > 0;
        }

        if ($result = UsersModel::getUnreadNotificationsCount($this->id)) {
            $this->unreadNotificationsCount = $result;
        }

        return $this->unreadNotificationsCount > 0;
    }

    public function notifications()
    {
        if (!isset($this->notifications)) {
            if (!($notifications = UsersModel::getNotifications($this->id))) {
                return array();
            }

            $this->notifications = $notifications;
        }

        return $this->notifications;
    }

    public function sendNotification($to, $subject, $text)
    {
        return UsersModel::sendNotification($this->id, $to, $subject, $text);
    }

    public function results()
    {
        if (isset($this->results)) {
            return $this->result;
        }

        if ($results = UsersModel::getResults($this->guest_id)) {
            $this->results = $results;
            return $results;
        }
        
        return array();
    }

    public function seenNotification($notificationId)
    {
        return UsersModel::seenNotification($this->id, $notificationId);
    }
}