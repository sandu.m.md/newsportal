<?php

class Loader
{
    public static function load($view, $data = null)
    {
        if ($data != null) {
            $data = json_decode(json_encode($data));
        }
        
        require("protected/views/$view" . "View.php");
    }

    public static function loadC($component, $data = null)
    {
        if ($data != null) {
            $data = json_decode(json_encode($data));
        }
        
        require("protected/views/$component" . "CView.php");
    }
}


?>