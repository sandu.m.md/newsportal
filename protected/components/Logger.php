<?php

class Logger
{
    /** @var Config */
    private $conf;

    private $errorFile;
    private $requestFile;

    function __construct($conf)
    {
        $this->conf = $conf;

        $this->errorFile = @fopen($this->conf->error->file, "a+");
        $this->requestFile = @fopen($this->conf->request->file, "a+");
    }

    public function error($msg)
    {
        if (is_object($msg) || is_array($msg)) {
            $msg = json_encode($msg);
        }

        $class = debug_backtrace()[1]['class'];
        $method = debug_backtrace()[1]['function'];
        $msgs = trim(preg_replace('/\s\s+/', " ", $msg));

        if ($this->conf->error->enabled) {
            fwrite($this->errorFile, $this->getIp() . " " . date("Y-m-d H:i:s") . " [$class] [$method] [$msg]\n");
        }
    }

    public function request($time, $user, $root, $respCode)
    {
        if ($this->conf->request->enabled) {
            fwrite($this->requestFile, $this->getIp() . " " . date("Y-m-d H:i:s") . " [".substr($time, 0, 4)."s] [$user] [$root] [$respCode]\n");
        }
    }

    private function getIp()
    {
        return App::getIp();
    }
}

?>