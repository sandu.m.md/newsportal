<?php

class Config
{
	private $conf;

	function __construct()
    {
		$this->conf = json_decode(json_encode(array(
			"debug" => true,

			"user" => array(
				"notifications" => array(
					"enabled" => true
				)
			),

			"themes" => array(
				"dark" => array(
					"bg" => array(
						"color" => "bg-dark",
						"color_alt" => "bg-dark",
						"color_footer" => "bg-dark"
					),
					"text" => array(
						"color" => "text-light",
						"hover" => "hover-text-light",
						"color_alt" => "text-lighter",
						"hover_alt" => "text-light",
						"color_footer" => "text-light"
					),
				),
				"light" => array(
					"bg" => array(
						"color" => "bg-light",
						"color_alt" => "bg-dark",
						"color_footer" => "bg-dark"
					),
					"text" => array(
						"color" => "text-dark",
						"hover" => "hover-text-dark",
						"color_alt" => "text-dark",
						"hover_alt" => "hover-text-dark",
						"color_footer" => "text-light"
					)
				)
			),

			"db" => array(
				"host" => "127.0.0.1",
				"user" => "wdtpl",
				"pass" => "saniok2000",
				"dbname" => "newsportal"
			),

			"ads" => array(
				"enabled" => false
			),

			"db" => array(
				"host" => "127.0.0.1",
				"user" => "wdtpl",
				"pass" => "saniok2000",
				"dbname" => "newsportal"
			),

			// "db" => array(
			// 	"host" => "remotemysql.com",
			// 	"user" => "MneGL5Zcjt",
			// 	"pass" => "CopYtaFOHY",
			// 	"dbname" => "MneGL5Zcjt"
			// ),

			// "db" => array(
			// 	"host" => "remotemysql.com",
			// 	"user" => "MeLeO3ULDs",
			// 	"pass" => "dNhUzXPvqx",
			// 	"dbname" => "MeLeO3ULDs"
			// ),
			
			"session" => array(
				"duration" => "- 5 hours"
			),

			"logger" => array(
				"error" => array(
					"enabled" => true,
					"file" => "err.log"
				),

				"request" => array(
					"enabled" => true,
					"file" => "req.log"
				)
			),

			"quiz" => array(
				"duration" => 8, // seconds
				"correct_answer_timeout" => 0, // seconds
				"next_question_timeout" => 2, // seconds
			),
		)));
	}
    
    public function __get($property)
    {
        return $this->conf->{$property};
    }
}

?>