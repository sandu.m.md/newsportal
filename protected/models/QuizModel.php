<?php

class QuizModel
{

    public static function getActiveById($id)
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) a WHERE `id` = $id AND `active` = 1 AND `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return array_shift($result);
        }

        return false;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) a WHERE `id` = $id";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return array_shift($result);
        }

        return false;
    }

    public static function getActiveQuestions($quizId)
    {
        $sql = "SELECT * FROM `quiz_questions` WHERE `quiz_id` = $quizId AND `active` = 1 AND `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getQuestions($quizId)
    {
        $sql = "SELECT * FROM `quiz_questions` WHERE `quiz_id` = $quizId AND `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return array();
    }

    public static function getNextQuizzes($quizId)
    {
        $guestId = App::$guestId;

        $sql = "SELECT q.id, q.name, ifnull(q.datetime,'2002-05-12 00:00:00') datetime, q.correct_percentage answered, qq.cnt questions, q.passed passes FROM
        (SELECT q.id, q.name, qs.datetime, qs.correct_percentage, q.passed FROM `quiz` q LEFT JOIN (SELECT quiz_id, max(datetime_stop) datetime, max(correct_percentage) correct_percentage FROM `quiz_sessions` where guest_id = $guestId group by quiz_id) qs on qs.quiz_id = q.id WHERE q.removed <> 1 AND q.active = 1) q
        INNER JOIN 
        (SELECT `quiz_id`, COUNT(if (`active` = 1 AND `removed` = 0, 1, NULL)) `cnt` FROM `quiz_questions` GROUP BY `quiz_id`) as qq
        ON q.id = qq.quiz_id
        WHERE q.id <> $quizId
        ORDER BY q.datetime ASC, q.passed DESC";


        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            $arr = array();
            $i = 0;

            foreach ($result as $row) {
                $arr[$i++] = $row;
            }
            
            return $arr;
        }

        return false;
    }

    public static function getAllActiveExcept($id)
    {
        $sql = "SELECT * FROM `quiz` WHERE `id` != $id AND `active` = 1 AND `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getAll()
    {
        $sql = "SELECT * FROM `quiz` WHERE `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function start($id)
    {
        $sql = "UPDATE `quiz` SET `started` = `started` + 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        $sql = "INSERT INTO `quiz_sessions`(`quiz_id`, `guest_id`) VALUES ($id, ".App::$guestId.")";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function pass($id, $quizId, $correct_answers, $correct_percentage)
    {
        $sql = "UPDATE `quiz` SET `passed` = `passed` + 1 WHERE `id` = $quizId";

        if (!App::$db->query($sql)) {
            return false;
        }

        $sql = "UPDATE `quiz_sessions` SET `datetime_stop` = now(), `correct_answers` = $correct_answers, `correct_percentage` = $correct_percentage, `passed` = 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function answer($sessionId, $qid, $answer, $duration)
    {
        $sql = "UPDATE `quiz_questions` SET `choosed_$answer` = `choosed_$answer` + 1 WHERE `id` = $qid";

        if (!App::$db->query($sql)) {
            return false;
        }

        $sql = "INSERT INTO `quiz_question_answers` (`duration`, `question_id`, `guest_id`, `answer`, `quiz_session_id`) VALUES ($duration, $qid, ".App::$guestId.", $answer, $sessionId);";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function disable($id)
    {
        $sql = "UPDATE `quiz` SET `active` = 0 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function enable($id)
    {
        $sql = "UPDATE `quiz` SET `active` = 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function disableQuestion($id)
    {
        $sql = "UPDATE `quiz_questions` SET `active` = 0 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function enableQuestion($id)
    {
        $sql = "UPDATE `quiz_questions` SET `active` = 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function insert($userId, $title, $cover)
    {
        $title = str_replace("'", "\'", $title);
        $cover = str_replace("'", "\'", $cover);

        $sql = "INSERT INTO `quiz`(`name`, `cover`, `user_id`) VALUES ('$title', '$cover', $userId)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function update($id, $title, $cover)
    {
        $title = str_replace("'", "\'", $title);
        $cover = str_replace("'", "\'", $cover);

        $sql = "UPDATE `quiz` SET `name` = '$title', `cover` = '$cover' WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function updateQuestion($qid, $question, $answer1, $answer2, $answer3, $correct_answer)
    {
        $question = str_replace("'", "\'", $question);
        $answer1 = str_replace("'", "\'", $answer1);
        $answer2 = str_replace("'", "\'", $answer2);
        $answer3 = str_replace("'", "\'", $answer3);

        $sql = "UPDATE `quiz_questions` SET `question` = '$question', `answer_1` = '$answer1', `answer_2` = '$answer2', `answer_3` = '$answer3', `correct_answer` = $correct_answer WHERE `id` = $qid";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function insertQuestion($quiz_id, $question, $answer1, $answer2, $answer3, $correct_answer, $user_id)
    {
        $question = str_replace("'", "\'", $question);
        $answer1 = str_replace("'", "\'", $answer1);
        $answer2 = str_replace("'", "\'", $answer2);
        $answer3 = str_replace("'", "\'", $answer3);
        
        $sql = "INSERT INTO `quiz_questions`(`quiz_id`, `question`, `answer_1`, `answer_2`, `answer_3`, `correct_answer`, `user_id`, `active`) VALUES ($quiz_id, '$question', '$answer1', '$answer2', '$answer3', $correct_answer, $user_id, ".($user_id?1:0).")";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function removeQuestion($qid)
    {   
        $sql = "UPDATE `quiz_questions` SET `removed` = 1 WHERE `id` = $qid";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function remove($quizId)
    {   
        $sql = "UPDATE `quiz` SET `removed` = 1 WHERE `id` = $quizId";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function view($quizId)
    {
        $guestId = App::$guestId;
        $sql = "INSERT INTO `quiz_views`(`quiz_id`, `guest_id`) VALUES ($quizId, $guestId)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function players()
    {
        $sql = "SELECT * FROM `quiz_players` LIMIT 50";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function stats()
    {
        $sql = "SELECT * FROM `quiz_stats`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return array_shift($result);
        }

        return false;
    }

    public static function sess()
    {
        $sql = "SELECT * FROM `quiz_sess`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getRatingScale($ratingScaleId)
    {
        $sql = "SELECT * FROM `quiz_rating_scales` WHERE `id` = $ratingScaleId";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return array_shift($result);
        }

        return false;
    }

    public static function isAllowedToUser($quizId, $userId)
    {
        $sql = "SELECT COUNT(*) `cnt` FROM `quiz` WHERE `id` = $quizId AND `user_id` = $userId";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result)) {
            return array_shift($result)["cnt"] == "1" ? true : false;
        }
    }

    public static function getTop()
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) a WHERE `active` = 1 AND `removed` = 0 ORDER BY `passed` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getNewest()
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) a WHERE `active` = 1 AND `removed` = 0 ORDER BY `datetime_created` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getRecommended()
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) `fq` WHERE `active` = 1 AND `removed` = 0 AND `fq`.`id` NOT IN (SELECT DISTINCT `quiz_id` FROM `quiz_sessions` WHERE `guest_id` = ".App::$guestId." AND `passed` = 1) ORDER BY `passed` DESC, `datetime_created` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public static function getRecommendedExcluding($id)
    {
        $sql = "SELECT * FROM (select `q`.`id` AS `id`,`q`.`name` AS `name`,`q`.`active` AS `active`,`q`.`removed` AS `removed`,`q`.`passed` AS `passed`,`q`.`cover` AS `cover`,`q`.`datetime` AS `datetime_created`,`q`.`user_id` AS `user_id`,`q`.`checked` AS `checked`,`q`.`approved` AS `approved`,`qq`.`questions_count` AS `questions_count`,`qs`.`unique_passed` AS `unique_passed`,`qs`.`correct_percentage` AS `unique_passed_percentage`,`qs`.`medium_duration` AS `unique_medium_duration`,`qw`.`views` AS `views`,`qw`.`unique_views` AS `unique_views`,`u`.`nick` AS `owner_nick` from (((( .`quiz` `q` left join (select  .`quiz_questions`.`quiz_id` AS `quiz_id`,count( .`quiz_questions`.`id`) AS `questions_count` from  .`quiz_questions` where  .`quiz_questions`.`active` = 1 and  .`quiz_questions`.`removed` = 0 group by  .`quiz_questions`.`quiz_id`) `qq` on(`q`.`id` = `qq`.`quiz_id`)) left join (select `a`.`quiz_id` AS `quiz_id`,count(`a`.`id`) AS `unique_passed`,avg(`a`.`correct_percentage`) AS `correct_percentage`,avg(timestampdiff(SECOND,`a`.`datetime_start`,`a`.`datetime_stop`)) AS `medium_duration` from (select  .`quiz_sessions`.`id` AS `id`, .`quiz_sessions`.`datetime_start` AS `datetime_start`, .`quiz_sessions`.`datetime_stop` AS `datetime_stop`, .`quiz_sessions`.`quiz_id` AS `quiz_id`, .`quiz_sessions`.`guest_id` AS `guest_id`, .`quiz_sessions`.`correct_answers` AS `correct_answers`, .`quiz_sessions`.`correct_percentage` AS `correct_percentage`, .`quiz_sessions`.`passed` AS `passed` from  .`quiz_sessions` where  .`quiz_sessions`.`passed` = 1 group by  .`quiz_sessions`.`guest_id`, .`quiz_sessions`.`quiz_id`) `a` group by `a`.`quiz_id`) `qs` on(`q`.`id` = `qs`.`quiz_id`)) left join (select  .`quiz_views`.`quiz_id` AS `quiz_id`,count( .`quiz_views`.`id`) AS `views`,count(distinct  .`quiz_views`.`guest_id`) AS `unique_views` from  .`quiz_views` group by  .`quiz_views`.`quiz_id`) `qw` on(`q`.`id` = `qw`.`quiz_id`)) left join  .`users` `u` on(`q`.`user_id` = `u`.`id`))) `fq` WHERE `active` = 1 AND `fq`.`id` <> $id AND `removed` = 0 AND `fq`.`id` NOT IN (SELECT DISTINCT `quiz_id` FROM `quiz_sessions` WHERE `guest_id` = ".App::$guestId." AND `passed` = 1) ORDER BY `passed` DESC, `datetime_created` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result) > 0) {
            return $result;
        }

        return false;
    }
}


?>