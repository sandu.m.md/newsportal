<?php 

class PostsModel
{
    public static function getAll($page = 1, $limit = 100)
    {
        $sql = "SELECT * FROM `posts`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `posts` WHERE `id` = $id";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return array_shift($result);
    }

    public static function getAllActive()
    {
        $sql = "SELECT * FROM `posts` WHERE `active` = 1";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function disable($id)
    {
        $sql = "UPDATE `posts` SET `active` = 0 WHERE `id` = $id";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return true;
    }

    public static function enable($id)
    {
        $sql = "UPDATE `posts` SET `active` = 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function remove($id)
    {
        $sql = "UPDATE `posts` SET `removed` = 1, `datetime` = NOW(), `active` = 0 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function restore($id)
    {
        $sql = "UPDATE `posts` SET `removed` = 0, `dateteime` = NOW() WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function insert($title, $cover, $body, $active)
    {
        $sql = "INSERT INTO `posts`(`title`, `cover`, `body`, `active`) VALUES ('$title', '$cover', '$body', $active)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function update($id, $title, $cover, $body)
    {
        $title = str_replace("'", "\'", trim($title));
        $cover = str_replace("'", "\'", trim($cover));
        $body = str_replace("'", "\'", trim($body));

        $sql = "UPDATE `posts` SET `title` = '$title', `cover` = '$cover', `body` = '$body' WHERE `id` = $id";
        // $sql = "INSERT INTO `news`(`title`, `cover`, `body`, `active`) VALUES ('$title', '$cover', '$body', $active)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function like($id)
    {
        $sql = "UPDATE `posts` SET `likes` = `likes` + 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function unlike($id)
    {
        $sql = "UPDATE `posts` SET `likes` = `likes` - 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function rate($id)
    {
        $sql = "UPDATE `posts` SET `rates` = `rates` + 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function getAllActiveMostViewed($limit = 10)
    {
        $sql = "SELECT * FROM `posts` WHERE `active` = 1 AND `removed` = 0 ORDER BY `views` DESC, `datetime` DESC LIMIT $limit";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getAllNewest($page = 1, $limit = 100)
    {
        $sql = "SELECT * FROM `posts` WHERE `removed` = 0 ORDER BY `datetime` DESC LIMIT " . (($page-1) * $limit) . ",$limit";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getAllRemoved($page = 1, $limit = 100)
    {
        $sql = "SELECT * FROM `posts` WHERE `removed` = 1 ORDER BY `datetime` DESC LIMIT " . (($page-1) * $limit) . ",$limit";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getAllActiveNewest($page = 1, $limit = 100)
    {
        $sql = "SELECT * FROM `posts` WHERE `active` = 1 AND `removed` = 0 ORDER BY `datetime` DESC LIMIT " . (($page-1) * $limit) . ",$limit";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getAllActiveTopApreciated($page, $limit)
    {
        $sql = "SELECT * FROM `posts` WHERE `active` = 1 AND `removed` = 0 ORDER BY `likes` DESC, `views` DESC LIMIT " . (($page-1) * $limit) . ",$limit";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return $result;
    }

    public static function getLikesSum()
    {
        $sql = "SELECT sum(`likes`) `sum` FROM `posts`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return array_shift($result)["sum"];
    }

    public static function getViewsSum()
    {
        $sql = "SELECT sum(`views`) `sum` FROM `posts`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return array_shift($result)["sum"];
    }

    public static function getDislikesSum()
    {
        $sql = "SELECT sum(`rates` - `likes`) `sum` FROM `posts`";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        return array_shift($result)["sum"];
    }

    public static function incrementViews($id)
    {
        $sql = "UPDATE `posts` SET `views` = `views` + 1 WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }    
}


?>