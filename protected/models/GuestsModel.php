<?php 

class GuestsModel
{
    public static function insert($ip)
    {
        $sql = "INSERT INTO `guests`(`ip`) VALUES ('$ip');";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `guests` WHERE `id` = $id";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (!is_array($result)) {
            return false;
        }

        if (count($result) > 0) {
            return array_shift($result);
        }

        return false;
    }

    public static function update($id)
    {
        $sql = "UPDATE `guests` SET `last_datetime` = NOW() WHERE `id` = $id";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

}

?>