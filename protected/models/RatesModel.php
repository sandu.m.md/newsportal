<?php 

class RatesModel
{
    public static function like($guestId, $newsId)
    {
        $sql = "INSERT INTO `rates`(`guest_id`, `news_id`, `like`) VALUES ($guestId, $newsId, 1)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function unlike($guestId, $newsId)
    {
        $sql = "INSERT INTO `rates`(`guest_id`, `news_id`, `like`) VALUES ($guestId, $newsId, 0)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function getLastGuestIdAndNewsId($guestId, $newsId)
    {
        $sql = "SELECT * FROM `rates` WHERE `guest_id` = $guestId AND `news_id` = $newsId ORDER BY `datetime` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (!is_array($result)) {
            return false;
        }

        if (count($result) == 0) {
            return false;
        }
        
        return array_shift($result);
    }
}


?>