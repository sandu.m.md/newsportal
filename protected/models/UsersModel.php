<?php 

class UsersModel
{

    public static function getByUsername($username)
    {
        $username = str_replace("@", "\@", $username);

        $sql = "SELECT * FROM `users` WHERE `username` = '$username'";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result)) {
            return array_shift($result);
        }

        return false;
    }

    public static function getById($id)
    {
        $sql = "SELECT * FROM `users` WHERE `id` = '$id'";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result)) {
            return array_shift($result);
        }

        return false;
    }

    public static function updateLastLogin($id)
    {
        $sql = "UPDATE `users` SET `datetime_last_login` = NOW(), datetime_last_access = NOW() WHERE `id` = '$id'";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function updateLastAccess($id)
    {
        $sql = "UPDATE `users` SET datetime_last_access = NOW() WHERE `username` = '$id'";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function startSession($id, $ip)
    {
        $sql = "INSERT INTO `users_sessions`(`user_id`, `ip`) VALUES ($id, '$ip')";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function register($email, $nick, $password, $guestId)
    {
        $email = str_replace("'", "\'", $email);
        $nick = str_replace("'", "\'", $nick);
        $password = str_replace("'", "\'", $password);
        $sql = "INSERT INTO `users`(`email`, `username`, `password`, `nick`, `guest_id`) VALUES ('$email', '$email', '".md5($password)."', '$nick', $guestId)";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }

    public static function getSessionById($id)
    {
        $sql = "SELECT * FROM `users_sessions` WHERE `id` = '$id'";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        if (is_array($result) && count($result)) {
            return array_shift($result);
        }

        return false;
    }

    public static function updateSesssion($id)
    {
        $sql = "UPDATE `users_sessions` SET datetime_last_access = NOW() WHERE `id` = '$id'";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function getRating($id)
    {
        $res = array();

        $sql = "SELECT `rating` FROM `users_rating` WHERE `id` = $id";
        
        if (!($result = App::$db->query($sql))) {
        }

        if (is_array($result) && count($result)) {
            $res["rating"] = array_shift($result)["rating"];
        }        

        $sql = "SELECT count(id) `place` FROM `users_rating` WHERE `rating` >= (SELECT `rating` FROM `users_rating` WHERE `id` = $id)";
        
        if (!($result = App::$db->query($sql))) {
        }

        if (is_array($result) && count($result)) {
            $res["place"] = array_shift($result)["place"];
        }

        $sql = "SELECT COUNT(*) `total`, COUNT(if(`id` = $id, 1, NULL)) `ok` FROM `users_rating`";
        
        if (!($result = App::$db->query($sql))) {
        }

        if (is_array($result) && count($result)) {
            $res["total"] = end($result)["total"];
            $res["ok"] = end($result)["ok"];
        }
        
        $sql = "SELECT `percentage_avg`, `passed_count` FROM `users_rating` WHERE `id` = $id";
        
        if (!($result = App::$db->query($sql))) {
        }

        if (is_array($result) && count($result)) {
            $res["passed_count"] = end($result)["passed_count"];
            $res["percentage"] = end($result)["percentage_avg"];
        }

        return $res;
    }

    public static function getRatingTop()
    {        
        $sql = "SELECT `u`.`id`, `ur`.`datetime_start`, `ur`.`datetime_stop`, `ur`.`rating`, `ur`.`passed_count`, `ur`.`percentage_avg`, `u`.`nick` FROM `users_rating` `ur` INNER JOIN `users` `u` ON `ur`.`id` = `u`.`id` ORDER BY `rating` DESC";
        
        if (!($result = App::$db->query($sql))) {
            return array();
        }

        if (is_array($result) && count($result)) {
            return $result;
        }

        return array();
    }

    public static function getPassedQuizzes($id)
    {
        $sql = "select `q`.`id`, `q`.`name`, qq.cnt questions, max(if (`qs`.`passed`=1, `qs`.`correct_percentage`, null)) answered, `qs`.`datetime_stop` from `users` `u` inner join `quiz_sessions` `qs` on `u`.`guest_id` = `qs`.`guest_id` inner join `quiz` `q` on `q`.`id` = `qs`.`quiz_id` inner join (select `quiz_id`, count(if(active=1 and removed=0,1,null)) cnt from `quiz_questions` group by `quiz_id`) qq on qq.quiz_id = q.id where `q`.`active` = 1 AND `q`.`removed` = 0 AND `u`.`id` = 1 AND `qs`.`passed` = $id group by `q`.`id` order by `qs`.`datetime_stop` DESC";

        if (!($result = App::$db->query($sql))) {
            return false;
        }

        // App::$log->error(json_encode($result));

        if (is_array($result) && count($result)) {
            return $result;
        }

        return false;
    }

    public static function getRecommendedQuizzes($id)
    {
        $sql = "SELECT * FROM `quiz_active` `qa` WHERE `qa`.`id` NOT IN (SELECT DISTINCT `quiz_id` FROM `quiz_sessions` `qs` INNER JOIN `users` `u` ON `u`.`guest_id` = `qs`.`guest_id` WHERE `u`.`id` = $id) ORDER BY `passed` DESC, `datetime` DESC";

        if (!($result = App::$db->query($sql))) {
            return array();
        }

        if (is_array($result) && count($result)) {
            return $result;
        }

        return array();
    }

    public static function getQuizzes($id)
    {
        $sql = "SELECT * FROM `full_quiz` WHERE `user_id` = $id AND `removed` = 0";

        if (!($result = App::$db->query($sql))) {
            return array();
        }

        if (is_array($result) && count($result)) {
            return $result;
        }

        return array();
    }

    public static function getResults($guestId)
    {
        $sql = "SELECT
                    `qs`.`datetime_start` `datetime_start`,
                    `qs`.`datetime_stop` `datetime_stop`,
                    `qs`.`quiz_id` `id`,
                    `qs`.`correct_answers` `correct_answers`,
                    `qs`.`correct_percentage` `correct_percentage`,
                    `q`.`name` `name`,
                    `q`.`passed` `passed_count`,
                    `qq`.`questions_count`,
                    `qw`.`views`,
                    `qs2`.`unique_players`
                FROM
                    `quiz_sessions` `qs`
                INNER JOIN `quiz` `q` ON `qs`.`quiz_id` = `q`.`id`
                INNER JOIN (SELECT `quiz_id`, COUNT(if(`active` = 1 AND `removed` = 0, 1, NULL)) `questions_count` FROM `quiz_questions` GROUP BY `quiz_id`) `qq` ON `q`.`id` = `qq`.`quiz_id`
                INNER JOIN (SELECT `quiz_id`, COUNT(`id`) `views` FROM `quiz_views` GROUP BY `quiz_id`) `qw` ON `q`.`id` = `qw`.`quiz_id`
                INNER JOIN (SELECT `quiz_id`, COUNT(DISTINCT `guest_id`) `unique_players` FROM `quiz_sessions` WHERE `passed` = 1 GROUP BY `quiz_id`) `qs2` ON `q`.`id` = `qs2`.`quiz_id`
                WHERE
                    `qs`.`passed` = 1
                AND guest_id = $guestId
                GROUP BY
                    `qs`.`quiz_id`,
                    `qs`.`guest_id`";

        if (!($result = App::$db->query($sql))) {
            return array();
        }

        if (is_array($result) && count($result)) {
            return $result;
        }

        return array();
    }

    public static function getNotifications($id)
    {
        $sql = "SELECT * FROM `full_users_notifications` WHERE `to_user_id` = $id ORDER BY `datetime_sent` DESC";

        if (!($result = App::$db->query($sql))) {
            return array();
        }

        if (is_array($result) && count($result)) {
            return $result;
        }

        return array();
    }

    public static function getUnreadNotificationsCount($id)
    {
        $sql = "SELECT COUNT(if(`seen` = 0, 1, NULL)) `cnt` FROM `users_notifications` WHERE `to_user_id` = $id";

        if (!($result = App::$db->query($sql))) {
            return 0;
        }

        if (is_array($result) && count($result)) {
            return array_shift($result)["cnt"];
        }

        return 0;
    }

    public static function seenNotification($userId, $notificationId)
    {
        $sql = "UPDATE `users_notifications` SET `seen` = 1 WHERE `id` = $notificationId AND `to_user_id` = $userId";

        if (!App::$db->query($sql)) {
            return false;
        }

        return true;
    }

    public static function sendNotification($from, $to, $subject, $text)
    {
        $sql = "INSERT INTO `users_notifications`(`from_user_id`, `to_user_id`, `subject`, `text`) VALUES ($from, $to, '$subject', '$text')";

        if (!App::$db->query($sql)) {
            return false;
        }

        return App::$db->insertedId();
    }
}



?>