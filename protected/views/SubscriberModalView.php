<!-- The Modal -->
<form id="subscribe-form" onsubmit="return subscribe();">
    <div class="modal mt-5" id="subscribe-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="font-weight-light text-dark">Cele mai noi știri în fiecare zi</h3>
                    <button type="button" class="close" data-dismiss="modal" id="upload-img-close">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="text" class="form-control" name="email" placeholder="Adresa de email" required>

                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Trimite">
                </div>

                <!-- Hidden -->
                <input type="hidden" name="img-type">
            </div>
        </div>
    </div>
</form>
<script>
    function subscribe() {
        $.post("/api/subscribe", $("#subscribe-form").serializeArray(), function(data){
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    console.log("Good");
                } else {
                    console.log("Bad");
                }
            } catch (ex) {
                console.log(ex);
            }
        })
        
        return false;
    }
</script>