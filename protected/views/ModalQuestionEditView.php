<div class="modal mt-5" id="modal-question-edit">
    <form id="modal-question-edit-form" onsubmit="return saveQuestion();">
        <div class="modal-dialog">
            <div class="modal-content bg-dark text-light">
                <!-- Modal Header -->
                <div class="modal-header" style="border-color: rgba(0, 0, 0, 0.5)">
                    <h3 class="font-weight-light">Editează câmpurile</h3>
                    <button type="button" class="close text-light" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="border-color: rgba(0, 0, 0, 0.5)">
                    <input type="hidden" value="0" name="question-id" id="modal-question-edit-question-id">
                    <input type="hidden" value="0" name="quiz-id" id="modal-question-edit-quiz-id">
                    <input type="hidden" value="<?php echo App::$user->id?>" name="user-id" id="modal-question-edit-user-id">

                    <h5 class="pb-1"><span class="bg-primary rounded text-light p-1 pl-2 pr-2">Textul întrebării</span></h5>
                    <input type="text" class="form-control" name="question" placeholder="Ce? Unde? Când?" required autocomplete="off">

                    <h5 class="pb-1 pt-4"><span class="bg-danger rounded text-light p-1 pl-2 pr-2">Variantele de răspuns</span></h5>
                    <input type="text" class="form-control mb-2" name="answer-1" id="modal-question-edit-answer-1" placeholder="Varianta 1" required autocomplete="off">
                    <input type="text" class="form-control mb-2" name="answer-2" id="modal-question-edit-answer-2" placeholder="Varianta 2" required autocomplete="off">
                    <input type="text" class="form-control mb-2" name="answer-3" id="modal-question-edit-answer-3" placeholder="Varianta 3" required autocomplete="off">

                    <h5 class="pb-1 pt-4"><span class="bg-success rounded text-light p-1 pl-2 pr-2">Varianta corectă</span></h5>

                    <select name="correct-answer" id="modal-question-edit-correct-answer" class="custom-select">
                        <option value="1">#1</option>
                        <option value="2">#2</option>
                        <option value="3">#3</option>
                    </select>                            
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer" style="border-color: rgba(0, 0, 0, 0.5)">
                    <input type="submit" class="btn btn-block btn-lg btn-success" value="Salvează">
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $("#modal-question-edit-correct-answer").children("[value=1]").html($("#modal-question-edit-answer-1").val());
    $("#modal-question-edit-correct-answer").children("[value=2]").html($("#modal-question-edit-answer-2").val());
    $("#modal-question-edit-correct-answer").children("[value=3]").html($("#modal-question-edit-answer-3").val());

    $("#modal-question-edit-answer-1").change(function(e){
        $("#modal-question-edit-correct-answer").children("[value=1]").html($(e.target).val());
    });

    $("#modal-question-edit-answer-2").change(function(e){
        $("#modal-question-edit-correct-answer").children("[value=2]").html($(e.target).val());
    });

    $("#modal-question-edit-answer-3").change(function(e){
        $("#modal-question-edit-correct-answer").children("[value=3]").html($(e.target).val());
    });

    function clearModalQuestionEdit(quizId) {
        $("[name=quiz-id]").val(quizId);
        $("[name=question-id]").val(0);
        $("[name=question]").val('');
        $("[name=answer-1]").val('');
        $("[name=answer-2]").val('');
        $("[name=answer-3]").val('');
        $("[name=correct-answer]").val(1);
        setTimeout(function(){$('[name=question]').focus();}, 100);
    }

    function fillModalQuestionEdit(questionId) {
        $("[name=question-id]").val(questionId);
        $("[name=quiz-id]").val($("#question-"+questionId+"-quiz-id").val());
        $("[name=question]").val($("#question-"+questionId+"-question").val());
        $("[name=answer-1]").val($("#question-"+questionId+"-answer-1").val());
        $("[name=answer-2]").val($("#question-"+questionId+"-answer-2").val());
        $("[name=answer-3]").val($("#question-"+questionId+"-answer-3").val());
        $("[name=correct-answer]").val($("#question-"+questionId+"-correct-answer").val());
        $("#modal-question-edit-correct-answer").children("[value=1]").html($("#modal-question-edit-answer-1").val());
        $("#modal-question-edit-correct-answer").children("[value=2]").html($("#modal-question-edit-answer-2").val());
        $("#modal-question-edit-correct-answer").children("[value=3]").html($("#modal-question-edit-answer-3").val());
        setTimeout(function(){$('[name=question]').focus();}, 100)
    }

    function saveQuestion() {
        $.post("/user/saveQuestion", $("#modal-question-edit-form").serializeArray(), function(data) {
            try {
                var resp = JSON.parse(data);

                if (resp.ok) {
                    location.reload();
                } else {
                    console.log("Bad");
                }
            } catch (ex) {
                console.log(ex.message);
            }
        });

        return false;
    }
</script>
