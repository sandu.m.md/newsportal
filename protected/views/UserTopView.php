<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jucătorii de top!</title>
    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            overflow-X: hidden;
        }
    </style>
</head>
<body class="bg-dark text-light">

    <div class="container-fluid">

        <!-- First place -->
        <div class="row" style="min-height: 100vh">
            <div class="col-md-12">
                <div class="row cover-image bg-1 rounded-bottom-xl shadow" style="min-height:15vh; background-position: bottom">

                    <!-- Topbar -->    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="d-flex p-3 w-100">
                                <div class="text-left" style="width:33.33vw;">
                                    <a href="/quiz" class="btn text-light hover-bg-darker"><i class="fa fa-home mr-1"></i>Home</a>
                                </div>
                                <div class="text-center" style="width:33.33vw;">
                                    <a href="/user/top" class="btn btn-lg text-light hover-text-light"><h2 class=" font-weight-light m-0 p-0"><i class="far fa-star mr-1"></i></h2></a>
                                </div>
                                <div class="text-right" style="width:33.33vw;">
                                <?php if (App::$user->isLoggedIn()) { ?>
                                        <a href="/user/home" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                                    <?php } else { ?>
                                        <a href="/user/login" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12"></div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    
                    <div class="col-md-4 m-3 position-relative" id="place-1">
                        <img src="/res/flw_square.png" class="img-fluid bg-darker">
                        <img id="flw-4" src="/res/flw-4.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(3)rotate(90deg);opacity:0;">
                        <img id="flw-3" src="/res/flw-3.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(3)rotate(90deg);opacity:0;">
                        <img id="flw-2" src="/res/flw-2.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(3)rotate(90deg);opacity:0;">
                        <div id="place-1-digit" class="position-absolute d-flex h-100 w-100 justify-content-center align-items-center text-warning" style="top:0;left:0;font-size:10rem;transform:scale(3);opacity:0;">
                            <span>1</span>
                        </div>
                        <img id="flw-1" src="/res/flw-1.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(3)rotate(90deg);opacity:0.4;">
                        <div id="place-1-text" class="position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="top:0;left:0;transform:scale(4);opacity:0;">
                            <span class="text-center">
                                <h2 class="m-0">
                                    <span style="white-space:no-wrap"><?php echo$data->top[0]->nick?></span>
                                </h2>
                                <h4 class="font-weight-light text-warning m-0">
                                    <?php echo (int)$data->top[0]->rating?> points
                                </h4>
                            </span>
                        
                        </div>
                    </div>
                    
                    <div class="col-md-4"></div>
                </div>
            </div>

            <div class="col-md-12 text-center">
                <a href="#main-content" id="scroll-down">
                    <div class="btn text-secondary hover-text-danger" id="top-scroll-down">
                        <i class="fa fa-angle-double-down display-4"></i>
                    </div>
                </a>
            </div>

            <div class="col-md-12"></div>
            <div class="col-md-12"></div>
        </div>

        <!-- Other places -->
        <div class="container pt-4" id="main-content">

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 p-0">
                    <h6 class="font-weight-light text-secondary text-center">
                        <i class="fa fa-info-circle"></i>
                        <?php if(App::$user->isLoggedIn()) {?>
                            <span>Ratingul se calculează în funcție de câte teste ați rezolvat și ce rezultat ați obținut la primele rezolvări ale testelor</span>
                        <?php } else { ?>
                            <span>Pentru a participa în rating trebuie să deții un cont de utilizător.<br>Te poți înregistra <a href="/user/register" class="btn-text-light">aici</a>.</span>
                        <?php } ?>
                    </h6>
                </div>
                <div class="col-md-3"></div>
            </div>            
            
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 p-0">
                    
                    <!-- 2nd -->
                    <div class="rounded-lg shadow bg-2nd p-3 d-flex mt-3 mb-3">
                        <div class="d-flex align-items-center" style="width: 3rem;">
                            <div class="position-relative text">
                                <img src="/res/top_2nd_1.png" class="img-fluid">
                                <h3 class="position-absolute d-flex w-100 h-100 justify-content-center align-items-center" style="top:0;left:0;">2</h3>
                            </div>
                        </div>
                        <div class="flex-fill d-flex align-items-center pl-3">
                            <h3 class="m-0"><?php echo$data->top[1]->nick?></h3>
                        </div>
                        <div class="d-flex align-items-center">
                            <h4 class="font-weight-light m-0">
                                <?php echo (int)$data->top[1]->rating?>p
                            </h4>
                        </div>
                    </div>

                    <!-- 3rd -->
                    <div class="rounded-lg shadow bg-silver p-3 d-flex mt-3 mb-3">
                        <div class="d-flex align-items-center" style="width: 3rem;">
                            <div class="position-relative">
                                <img src="/res/top_3rd_5.png" class="img-fluid">
                                <h3 class="position-absolute d-flex w-100 h-100 justify-content-center align-items-center" style="top:0;left:0;">3</h3>
                            </div>                            
                        </div>
                        <div class="flex-fill d-flex align-items-center pl-3">
                            <h3 class="m-0"><?php echo$data->top[2]->nick?></h3>
                        </div>
                        <div class="d-flex align-items-center">
                            <h4 class="font-weight-light m-0"><?php echo (int)$data->top[2]->rating?>p</h4> 
                        </div>
                    </div>

                    <!-- Others -->
                    <?php $i=3; foreach (array_slice($data->top, 3) as $user) { $i++?>
                        <div class="rounded-lg shadow bg-lighter p-3 d-flex mt-3 mb-3">
                            <div class="d-flex align-items-center" style="width: 3rem;">
                                <div class="position-relative">
                                    <img src="/res/top_others_1.png" class="img-fluid">
                                    <h3 class="position-absolute d-flex w-100 h-100 justify-content-center text-secondary align-items-center" style="top:0;left:0;"><?php echo $i?></h3>
                                </div>                            
                            </div>
                            <div class="flex-fill d-flex align-items-center pl-3">
                                <h3 class="m-0"><?php echo$user->nick?></h3>
                            </div>
                            <div class="d-flex align-items-center">
                                <h4 class="font-weight-light text-warning m-0"><?php echo (int)$user->rating?>p</h4>
                            </div>
                        </div>
                    <?php } ?>

                </div>
                <div class="col-md-3"></div>
            </div>
        </div>    
    </div>

    <?php Loader::load("Footer");?>

    <!-- Modal Question Edit -->
    <?php Loader::load("ModalQuestionEdit");?>
    <script>
        setTimeout(function(){
            animateRotate("#flw-4", "left", 0.1);
            animateRotate("#flw-3", "right", 0.05);
            animateRotate("#flw-2", "left", 0.15);
            animateRotate("#flw-1", "right", 0.1);
        }, 1100);
        
        animateFadeIn("#flw-4", 0.6, 0, 30);
        animateFadeIn("#flw-3", 0.6, 0.2, 60);
        animateFadeIn("#flw-2", 0.6, 0.4, 90);
        animateFadeIn("#flw-1", 0.6, 0.6, 120);
        animateFadeIn("#place-1-digit", 0.8, 0.7);
        animateFadeIn("#place-1-text", 0.8, 0.7);
        

        animateGoDownUp("#top-scroll-down", 2);
    </script>
</body>
</html>