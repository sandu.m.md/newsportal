<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php Loader::load("Requires");?>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-ajax-downloader@1.1.0/src/ajaxdownloader.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row p-2 bg-dark text-light sticky-top">
            <div class="col-md-*">
                <a href="/admin/live" class="btn btn-sm btn-danger">Live</a>
            </div>
            <div class="col-md-* ml-2">
                <a href="/posts/edit" class="btn btn-sm btn-success">Postare nouă</a>
            </div>
            <div class="col-md-* ml-2">
                <a href="/quiz/edit" class="btn btn-sm btn-success">Test nou</a>
            </div>
            <div class="col-md-* ml-auto">
                <a href="/admin/logout" class="btn btn-sm btn-secondary">Log out</a>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#all-news">Manager</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#all-quiz">Teste</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu2">Statistici</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#posts-removed">Postări șterse</a>
                    </li>
                </ul>

                <div class="tab-content mt-4">

                    <!-- Posts manager -->
                    <div id="all-news" class="container-fluid tab-pane fade">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Copertă</th>
                                    <th>Titlu</th>
                                    <th>Activă</th>
                                    <th>Vizualizări</th>
                                    <th>Editează</th>
                                    <th>Șterge</th>
                                </tr>
                            </thead>
                            <tbody id="posts">
                                <?php if(false) foreach(PostsModel::getAllNewest() as $post) {?>
                                    <tr id="post-<?php echo $post['id']?>">
                                        <td><?php echo $post['id']?></td>
                                        <td><img src="<?php echo $post['cover']?>" alt="<?php echo $post['title']?>" class="img-fluid" style="max-height: 50px;"></td>
                                        <td><?php echo $post['title']?></td>
                                        <td><input type="checkbox" data-toggle="toggle" <?php if($post['active'])echo 'checked';?> data-size="sm" data-style="ios" onchange="changeStatus(this,<?php echo $post['id']?>);"></td>
                                        <td class="text-center"><a href="/posts/view/<?php echo $post['id']?>" target="__blank"><h4><span class="badge bg-primary text-light"><i class="fa fa-eye"></i> <?php echo $post['views']?></span></h4></a></td>
                                        <td class="text-center"><h4><a href="/posts/edit/<?php echo $post['id']?>" target="__blank" class="fa fa-edit"></a></h4></td>
                                        <td class="text-center"><h4><div onclick="remove(<?php echo $post['id']?>)" target="__blank" class="fa fa-times text-danger" style="cursor:pointer"></a></h4></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <!-- Quiz manager -->
                    <div id="all-quiz" class="container-fluid tab-pane active">
                        <div class="row">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Titlu</th>
                                        <th><span class="badge badge-secondary"><i class="fa fa-question"></i></span></th>
                                        <th>Au început</th>
                                        <th>Au finisat</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="posts">
                                    <?php foreach (QuizModel::getAll() as $quiz) {?>
                                        <tr id="quiz-<?php echo $quiz["id"]?>">
                                            <td><?php echo $quiz['id']?></td>
                                            <td><?php echo $quiz['name']?></td>
                                            <td><?php echo count(QuizModel::getActiveQuestions($quiz["id"]))?></td>
                                            <td><?php echo $quiz["started"]?></td>
                                            <td><?php echo $quiz["passed"]?></td>
                                            <td><input type="checkbox" data-toggle="toggle" <?php if($quiz['active'])echo 'checked';?> data-size="sm" data-style="ios" onchange="changeQuizStatus(this,<?php echo $quiz['id']?>);"></td>
                                            <td><a href="/quiz/edit/<?php echo $quiz['id']?>" class="btn btn-sm text-primary"><i class="fa fa-pen"></i></a></td>
                                            <td><a href="/quiz/view/<?php echo $quiz['id']?>" class="btn btn-sm text-primary" title="Rulează" target="__blank"><i class="fa fa-play"></i></a></td>
                                            <td><button class="btn btn-sm text-danger" onclick="removeQuiz('Sigur doriți să ștergeți testul „<?php echo$quiz['name']?>”?', parseInt(<?php echo $quiz['id']?>));"><i class="fa fa-times"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <!-- Statistics -->
                    <div id="menu2" class="container tab-pane fade">
                        <div class="row">
                            <div class="col-md-4 p-3">
                                <div class="pt-5 pb-5 border rounded border border-primary text-primary text-center">    
                                    <span  class="display-4">
                                        <span class="fa fa-thumbs-up"></span> Likes<br><span class="badge bg-primary text-light" id="likes-sum">-</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 p-3">
                                <div class="pt-5 pb-5 border rounded border border-danger text-danger text-center">    
                                    <span class="display-4">
                                        <span class="fa fa-thumbs-down"></span> Dislikes<br><span class="badge bg-danger text-light" id="dislikes-sum">-</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 p-3">
                                <div class="pt-5 pb-5 border rounded border border-success text-success text-center">    
                                    <span class="display-4">
                                        <span class="fa fa-eye"></span> Vizualizări<br><span class="badge bg-success text-light" id="views-sum">-</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Removed posts -->
                    <div id="posts-removed" class="container-fluid tab-pane fade">
                        <table class="table table-hover table-responsive">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Copertă</th>
                                    <th>Titlu</th>
                                    <th>Vizualizări</th>
                                    <th>Editează</th>
                                    <th>Restabilește</th>
                                </tr>
                            </thead>
                            <tbody id="posts">
                                <?php if(false) foreach(PostsModel::getAllRemoved() as $post) {?>
                                    <tr id="post-removed-<?php echo $post['id']?>">
                                    <td><?php echo $post['id']?></td>
                                    <td><img src="<?php echo $post['cover']?>" alt="<?php echo $post['title']?>" class="img-fluid" style="max-height: 50px;"></td>
                                    <td><?php echo $post['title']?></td>
                                    <!-- <td><input type="checkbox" data-toggle="toggle" <?php if($post['active'])echo 'checked';?> data-size="sm" data-style="ios" onchange="changeStatus(this,<?php echo $post['id']?>);"></td> -->
                                    <td class="text-center"><h4><a href="/posts/view/<?php echo $post['id']?>" target="__blank" class="fa fa-eye"> <span class="badge bg-primary text-light"><?php echo $post['views']?></span></a></h4></td>
                                    <td class="text-center"><h4><a href="/posts/edit/<?php echo $post['id']?>" target="__blank" class="fa fa-edit"></a></h4></td>
                                    <td class="text-center"><h4><div onclick="restore(<?php echo $post['id']?>)" target="__blank" class="fa fa-trash-restore text-primary" style="cursor:pointer"></a></h4></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <form id="modal-upload-img-form" onsubmit="return uploadFile();">
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Încărcați o imagine</h4>
                            <button type="button" class="close" data-dismiss="modal" id="upload-img-close">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="modal-upload-img">
                                <label class="custom-file-label" name="modal-upload-img">Alegeți</label>
                            </div>
                        </div>
                        
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Ok">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    

    <script>
        
        // setInterval(function(){getStatistics();}, 1000);

        // getPosts("getAllNewest");

        function changeStatus(target, id)
        {
            var url = "/posts/enable/";
            if ($(target).attr("checked")) {
                url = "/posts/disable/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

        function changeQuizStatus(target, id)
        {
            var url = "/quiz/enable/";
            if ($(target).attr("checked")) {
                url = "/quiz/disable/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

        function uploadFile() {
            var url = "/api/uploadImg";
            var file_data = $('#modal-upload-img').prop('files')[0];   
            var form_data = new FormData($("#modal-upload-img-form")[0]);                  
            form_data.append('file', file_data);

            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data){
                    try {
                        var resp = JSON.parse(data);

                        if (resp.ok) {
                            var body = $("#post-body");
                            body.append('<img src="'+resp.body.filename+'" class="img-fluid">');
                        } else {
                            console.log("Bad");
                        }
                    } catch (ex) {
                        console.log(ex.message);
                    }
                }
            });
            $("#upload-img-close").trigger("click");

            return false;
        }

        function getStatistics() {
            $.post("/posts/getStatistics", {}, function(data){
                try {
                    // console.log(data);
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        $("#likes-sum").html(resp.body.likes);
                        $("#dislikes-sum").html(resp.body.dislikes);
                        $("#views-sum").html(resp.body.views);
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        }

        function getPosts(method = "getAll") {
            var container = $("#posts");
            $.post("/posts/"+method, {page: 1, limit: 100}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (resp.body !== true && resp.body !== false) {
                            
                            container.empty();
                            
                            $.each(resp.body, function(key, post) {
                                var html = '<tr id="post-'+post.id+'">'
                                        + '<td>'+post.id+'</td>'
                                        + '<td><img src="'+post.cover+'" alt="'+post.title+'" class="img-fluid" style="max-height: 50px;"></td>'
                                        + '<td>'+post.title+'</td>'
                                        // + '<td><div class="toggle btn btn-sm ios btn-light off" data-toggle="toggle" role="button" style="width: 46.325px; height: 30.8px;"><input type="checkbox" data-toggle="toggle" data-size="sm" data-style="ios" onchange="changeStatus(this,'+post.id+');"><div class="toggle-group"><label for="" class="btn btn-primary btn-sm toggle-on">On</label><label for="" class="btn btn-light btn-sm toggle-off">Off</label><span class="toggle-handle btn btn-light btn-sm"></span></div></div></td>'
                                        // + '<td><div class="toggle btn btn-primary btn-sm ios" data-toggle="toggle" role="button" style="width: 46.325px; height: 30.8px;"><input type="checkbox" '+(post.active=='1'?'checked':'')+' data-toggle="toggle" data-size="sm" data-style="ios" onchange="changeStatus(this,\'80\');"><div class="toggle-group"><label for="" class="btn btn-primary btn-sm toggle-on">On</label><label for="" class="btn btn-light btn-sm toggle-off">Off</label><span class="toggle-handle btn btn-light btn-sm"></span></div></div></td>'
                                        // + '<td><input type="checkbox" '+(post.active=='1'?'checked':'')+' data-toggle="toggle" data-size="sm" data-style="ios" onchange="changeStatus(this,\''+post.id+'\');"></td>'
                                        + '<td><div class="custom-control custom-switch"><input type="checkbox" '+(post.active=='1'?'checked':'')+' class="custom-control-input"></div></td>'
                                        + '<td class="text-center"><h4><a href="/posts/view/'+post.id+'" target="__blank" class="fa fa-eye"></a></h4></td>'
                                        + '<td class="text-center"><h4><a href="/posts/edit/'+post.id+'" target="__blank" class="fa fa-edit"></a></h4></td>'
                                        + '<td class="text-center"><h4><div onclick="remove('+post.id+')" target="__blank" class="fa fa-times text-danger" style="cursor:pointer"></a></h4></td>'
                                        + '</tr>';
                                container.append(html);
                            });
                        } else {
                            console.log("No data");
                        }
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                    console.log(data);
                }
            })
        }

        function remove(id) {
            $.post("/posts/remove/"+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        $("#post-"+id).remove();
                    } else {
                        console.log("Bad");
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

        function removeQuiz(msg, quiz_id) {
            console.log(quiz_id);
            if(confirm(msg)) {
                $.post('/quiz/remove/'+quiz_id, {}, function(data){
                    try {
                        var resp = JSON.parse(data);

                        if (resp.ok) {
                            location.reload();
                        } else {
                            alert("Ceva nu a funcționat corect");
                        }
                    } catch (ex) {
                        console.log(ex.message);
                    }
                });
            }
        }

        function restore(id) {
            $.post("/posts/restore/"+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        $("#post-removed-"+id).remove();
                    } else {
                        console.log("Bad");
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }
    </script>
    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();

        // Add the following code if you want the name of the file appear on select
        // $(".custom-file-input").on("change", function() {
        //     var fileName = $(this).val().split("\\").pop();
        //     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        // });
    </script>
</body>
</html>