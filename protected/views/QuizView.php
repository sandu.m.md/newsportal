<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test rapid! <?php echo $data->quiz->name?></title>
    <?php Loader::load("Requires");?>

    <style>
        body {
            /* overflow-x: hidden */
        }
    </style>
    
    <script>
        function addQuizHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[hidden-info]").css("opacity", "1");
                $(selector).children("[q-from]").addClass("text-info");
            }, function(){
                $(selector).children("[hidden-info]").css("opacity", "0");
                $(selector).children("[q-from]").removeClass("text-info");
            });
        }
    </script>
</head>
<body class="<?php echo App::$theme->bg->color." ".App::$theme->text->color?>">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0"></script>
    <a href="#top-anchor" id="top-anchor"></a>
    <img src="<?php echo $data->quiz->cover?>" alt="" id="cover" class="position-absolute" width="1" height="1">

    <div class="container-fluid">

        <!-- Preview -->
        <div id="quiz-preview" class="row" style="min-height: 100vh">
            <div class="col-md-12 bg-1 sticky-top rounded-bottom-xl" style="z-index:99;box-shadow: 0px 15px 30px #000000" onclick="$('#top-anchor').trigger('click');">
                <div class="row p-2">
                    <div class="col-md-*">
                        <a href="/quiz" class="btn text-light hover-bg-darker"><i class="fa fa-home mr-1"></i>Home</a>
                    </div>
                    <div class="col-md-* ml-auto">
                        <?php if (App::$user->isLoggedIn()) { ?>
                            <a href="/user/home" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                        <?php } else { ?>
                            <a href="/user/login" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-12 p-0 mt-3">
                    <div class="row d-flex justify-content-center align-items-center">
                        <h2 class="col-md-6 mx-auto text-light text-center m-0 font-weight-light mx-auto"><?php echo $data->quiz->name?></h2>
                    </div>
                </div>                    
            </div>

            <div class="col-md-12">
                <div class="row p-0">
                    <div class="col-md-6 p-0 m-0 mx-auto">
                        <div class="position-relative rounded-bottom-xl" style="height:35vh">
                            <div class="cover cover-image shadow position-absolute" style="left:0;top:-15px;height:35vh;background-image:url(<?php echo $data->quiz->cover?>)"></div>
                        </div>
                        
                        <div class="mx-auto mt-3 d-flex">
                            <div class="flex-fill d-flex flex-column justify-content-center align-items-center">
                                <div class="shadow position-relative d-flex justify-content-center align-items-center bg-special text-dark zoom hover-text-danger" style="border-radius:50%;height:4rem;width:4rem" title="Numărul de întrebări">
                                    <h4 class="m-0 text-light"><?php echo $data->quiz->questions_count;?></h4>
                                    <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.25;"><i class="fa fa-question text-warning"></i></h1>                                
                                </div>
                                <div>întrebări</div>
                            </div>
                            <div class="flex-fill d-flex flex-column justify-content-center align-items-center">
                                <div class="shadow position-relative d-flex justify-content-center align-items-center bg-ico-1 text-dark zoom hover-text-danger" style="border-radius:50%;height:4rem;width:4rem;overflow:hidden;" title="Numărul de jucători care au trecut testul">
                                    <h4 class="m-0 text-light"><?php echo $data->quiz->passed;?></h4>
                                    <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.25;"><i class="fa fa-user text-light"></i></h1>                                
                                </div>
                                <div>jucători</div>
                            </div>
                            <div class="flex-fill d-flex flex-column justify-content-center align-items-center">
                                <div class="shadow position-relative d-flex justify-content-center align-items-center bg-ico-2 text-dark zoom hover-text-danger" style="border-radius:50%;height:4rem;width:4rem" title="Timpul mediu în care a fost trecut testul(în secunde)">
                                    <h4 class="m-0 text-light"><?php echo (int)$data->quiz->unique_medium_duration;?></h4>
                                    <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.30;"><i class="fa fa-clock text-light"></i></h1>                                
                                </div>
                                <div>timp(s)</div>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>

            <div class="col-md-12 text-center sticky-bottom">
                <div class="text-center w-100">
                    <div id="btn-quiz-start" class="btn btn-danger btn-lg state-1" style="transition: all 1s;transform:scale(3);opacity:0" onclick="start_quiz();"><i class="fa fa-play"></i> Începe testul acum!</div>
                </div>
            </div>
            <div class="col-md-12 sticky-bottom">
                <div class="position-absolutes w-100 text-center" style="bottom:9rem;left:0">
                    <a href="#next-quizzes" onclick="highlightChildren('#recommended')">
                        <div class="btn text-secondary hover-text-danger" id="preview-scroll-down">
                            <i class="fa fa-angle-double-down display-4"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-12"></div>
        </div>

        <!-- Questions -->
        <div class="row">
            <div class="col-md-12">
                <?php $i=0;$cnt=count($data->q); foreach($data->q as $qid => $q) { $i++?>
                    <div id="q-<?php echo $i?>" class="row" style="min-height:100vh;display:none;">
                        <input type="hidden" id="q-<?php echo$i?>-correct" value="<?php echo $q->correct_answer?>">
                        <input type="hidden" id="q-<?php echo$i?>-id" value="<?php echo $q->id?>">

                        <div class="col-md-12">
                            <div class="row bg-1 rounded-bottom-xl" style="min-height:35vh;">
                                <div class="col-md-12 d-flex justify-content-center align-items-center">
                                    <!-- <div class="row">
                                        <div class="d-flex p-3 w-100">
                                            <div class="text-left" style="width:33.33vw;">
                                                <a href="/quiz" id="a-newest" class="btn text-light hover-bg-darker"><i class="fa fa-home mr-1"></i>Home</a>
                                            </div>
                                            <div class="text-center" style="width:33.33vw;">
                                                
                                            </div>
                                            <div class="text-right" style="width:33.33vw;">
                                            <?php if (App::$user->isLoggedIn()) { ?>
                                                    <a href="/user/home" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                                                <?php } else { ?>
                                                    <a href="/user/login" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row m-3">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <h1 class="font-weight-light text-center m-0 pb-4"><?php echo $q->question?></h1>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 text-center">
                            <div class="row">
                                <div class="col-md-3"></div>

                                <div class="col-md-6">
                                    <h5 class="font-weight-light text-secondary pb-4"><?php echo "$i/$cnt"?></h5>

                                    <div class="progress mb-4" style="height:10px;">
                                        <div class="progress-bar bg-info" style="width:100%;height:10px;font-size:17px;color:transparent;" id="q-<?php echo $i?>-timer"><?php echo App::$conf->quiz->duration?></div>
                                    </div>
                                    <div class="btn btn-lg btn-block btn-secondary" id="q-<?php echo$i?>-a-1" onclick="next_question(1)"><?php echo $q->answer_1?></div>
                                    <div class="btn btn-lg btn-block btn-secondary" id="q-<?php echo$i?>-a-2" onclick="next_question(2)"><?php echo $q->answer_2?></div>
                                    <div class="btn btn-lg btn-block btn-secondary" id="q-<?php echo$i?>-a-3" onclick="next_question(3)"><?php echo $q->answer_3?></div>
                                </div>
                                
                                <div class="col-md-3"></div>
                            </div>
                        </div>

                        <div class="col-md-12"></div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <!-- Totals -->
        <div class="row" id="totals" style="min-height: 100vh">
            <div class="col-md-12 cover-image bg-1 rounded-bottom-xl shadow">
                <div class="row p-3">
                    <div class="col-md-*">
                        <a href="/quiz" class="btn text-light hover-bg-darker"><i class="fa fa-home mr-1"></i>Home</a>
                    </div>

                    <div class="col-md-* ml-auto">
                        <?php if (App::$user->isLoggedIn()) { ?>
                            <a href="/user/home" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                        <?php } else { ?>
                            <a href="/user/login" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 position-relative">
                        <img src="/res/flw_back.png" class="img-fluid" style="opacity:0;">
                        <img id="totals-flw-3" src="/res/flw4.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(4) rotate(15deg);opacity:0;">
                        <img id="totals-flw-2" src="/res/flw2.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(4) rotate(52deg);opacity:0;">
                        <img id="totals-flw-1" src="/res/flw1.png" class="img-fluid position-absolute" style="top:0;left:0;transform:scale(4) rotate(90deg);opacity:0;">
                        <h1 class="font-weight-light d-flex position-absolute justify-content-center align-items-center h-100 px-5 w-100" style="top:0;left:0">
                            <div class="text-center" id="totals-score"></div>
                        </h1>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                    
            </div>

            <div class="col-md-12">
                <div class="fb-share-button" data-href="http://allmy.000webhostapp.com/" data-layout="button" data-size="large">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fallmy.000webhostapp.com%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a>
                </div>
                <div class="w-100 text-center">
                    <a href="#next-quizzes" onclick="highlightChildren('#recommended')">
                        <div class="btn text-secondary hover-text-danger" id="totals-scroll-down">
                            <i class="fa fa-angle-double-down display-4"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-12"></div>
        </div>
                
        <!-- Next quizzes -->
        <div class="container p-0" id="next-quizzes">
            <div class="pt-5">
                <?php Loader::Load("QuizCardSetC", array(
                    "title" => "Recomandări",
                    "quizzes" => QuizModel::getRecommendedExcluding($data->quiz->id),
                    "container" => "#recommended",
                    "showed" => 4
                ))?>
            </div>
        </div>
    </div>

    <!-- Subscribe modal -->
    <?php Loader::load("SubscriberModal")?>
    
    <!-- Register modal -->
    <?php Loader::load("ModalRegister")?>
    
    <!-- Footer -->
    <?php Loader::load("Footer", array(
        "feedback" => true,
        "user" => true,
        "quizEdit" => $data->quiz->id
    ))?>
                            
    <script>
        $(function () {
		    $('[data-toggle="tooltip"]').tooltip()
        })
    
        $("#cover").hide();
        $("#totals").hide();

        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 1000);
        });

        var timer = true;
        var current = 1;
        var correct_answers = 0;
        var count = parseInt(<?php echo count($data->q)?>);
        var seconds = -1;
        var sessionId = 0;

        var quiz_pass_id = 0;

        function start_quiz() {
            $.post("/quiz/start/<?php echo $data->quiz->id?>", {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        console.log("Good 1");
                        sessionId = resp.body.id;
                    } else {
                        console.log("Bad 1");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });

            $('#quiz-preview').hide();
            $("#next-quizzes").hide();
            $('#q-1').show();
            // $("#totals").show();
            start_question(current);
        }

        function start_question(qid) {
            timer = true;
            seconds = parseInt($("#q-"+qid+"-timer").html());
            var rate = (100/seconds)/20;
            var width = 100;

            // if(false)
            setInterval(function(){
                if (timer && width > 0) {
                    width -= rate;
                    $("#q-"+qid+"-timer").css("width", width + "%");
                    
                    if (width >= 25 && width < 50) {
                        $("#q-"+qid+"-timer").removeClass("bg-info").addClass("bg-warning");
                    } else if (width > 0 && width < 25) {
                        $("#q-"+qid+"-timer").removeClass("bg-warning").addClass("bg-danger");
                    } else if (width == 0) {
                        if (current == qid && timer) {
                            next_question(0);
                        }
                    }
                }
            }, 50);
        }

        function next_question(answer) {
            timer = false;
            $("#q-"+current+"-a-"+answer).removeClass("btn-secondary").addClass("btn-primary").addClass("active");
            if (answer != 1) $("#q-"+current+"-a-1").addClass("disabled");
            if (answer != 2) $("#q-"+current+"-a-2").addClass("disabled");
            if (answer != 3) $("#q-"+current+"-a-3").addClass("disabled");
            $("#q-"+current+"-a-1").attr("onclick", false);
            $("#q-"+current+"-a-2").attr("onclick", false);
            $("#q-"+current+"-a-3").attr("onclick", false);

            if (answer == parseInt($("#q-"+current+"-correct").val())) {
                setTimeout(function(){
                    $("#q-"+current+"-a-"+answer).removeClass("btn-secondary").addClass("btn-success");
                    correct_answers++;
                }, parseInt(<?php echo App::$conf->quiz->correct_answer_timeout?>)*1000);
            } else {
                setTimeout(function(){
                    $("#q-"+current+"-a-"+parseInt($("#q-"+current+"-correct").val())).removeClass("btn-secondary").addClass("btn-success");
                    $("#q-"+current+"-a-"+answer).removeClass("btn-secondary").addClass("btn-danger");
                }, parseInt(<?php echo App::$conf->quiz->correct_answer_timeout?>)*1000);
            }

            setTimeout(function(){
                if (current < count) {
                    $("#q-"+current).hide();
                    current++;
                    $("#q-"+current).show();
                    start_question(current);
                } else {
                    current = 0;
                    stop_quiz();
                }
            }, parseInt(<?php echo App::$conf->quiz->next_question_timeout?>)*1000);

            // send statistics
            var rate = (100/seconds);
            var per = $("#q-"+current+"-timer").width() / $("#q-"+current+"-timer").parent().width() * 100;
            var duration = parseInt(Math.round((100 - per) / rate));
            var qid = $("#q-"+current+"-id").val();
            $.post("/quiz/answer/"+qid, {duration: duration, answer: answer, sessionId:sessionId}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        console.log("Good 2");
                    } else {
                        console.log("Bad 2");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        }

        function stop_quiz() {
            $("#q-"+count).hide();
            // $("#totals-body").hide();
            var totals = $("#totals");

            var result = correct_answers * 100 / count;

            var msg = "";
            var html = "";
            var qualifier = "";

            if (result > 75) {
                msg = "Felicitări! Ești un geniu!";
                qualifier = "success";
            } else if (result > 45) {
                msg = "Felcitări! Ai dat dovadă de un bagaj foarte bun de cunoștințe!";
                qualifier = "primary";
            } else if (result > 20) {
                msg = "Ai obținut un rezultat bun, însă noi știm că poți mai bine!";
                qualifier = "warning";
            } else {
                msg = "Sunt sigur că la testul următor vei fi un geniu!";
                qualifier = "danger";
            }

            html += '<div class="text-center"><span class="bg-'+qualifier+' text-light badge" style="font-size:70px">'+(result+"").slice(0, 5)+'%</span></div>'
            html += '<h2 class="font-weight-light text-center pt-3">'+msg+'</h2>';

            <?php if(App::$user->isLoggedIn())echo "if(false)"?>
            setTimeout(function(){
                $("#modal-register-show").trigger("click");
            }, 4000);

            result = parseInt(result);

            $("#totals-score").html('Felicitări!<br><span class="text-warning">+'+result+' points</span>');

            totals.show();
            
            animateFadeIn("#totals-flw-3", 1);
            animateFadeIn("#totals-flw-2", 1, 0.3);
            animateFadeIn("#totals-flw-1", 1, 0.6);
            
            animateGoDownUp("#totals-scroll-down", 2);
            $("#next-quizzes").show();

            // send statistics
            $.post("/quiz/pass/<?php echo $data->quiz->id?>", {sessionId: sessionId, correct_answers: correct_answers, result: result}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        console.log("Good 3");
                    } else {
                        console.log("Bad 3");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        }
    </script>

    <script>
        $(document).ready(function() {
            animateGoDownUp("#preview-scroll-down", 2);
            $("#top-anchor").trigger("click");
            $("#btn-quiz-start").css({
                "transform": "scale(1)",
                "opacity": "1"
            });
        });

        function highlightChildren(selector, duration = 0.6) {
            var i = -1;

            $.each($(selector).children("*"), function(key, child) {
                i++;

                setTimeout(function() {
                    animateZoomDecrease("#"+$(child).attr("id"));
                }, i * duration * 2000);
            });
        }
    </script>
</body>
</html>