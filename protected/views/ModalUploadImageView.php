<div class="modal mt-5" id="modal-upload-image">
    <form id="modal-upload-image-form" onsubmit="return false;">
        <div class="modal-dialog">
            <div class="modal-content bg-dark text-light">
                <!-- Modal Header -->
                <div class="modal-header"  style="border-color: rgba(0, 0, 0, 0.5)">
                    <h4 class="modal-title">Încărcați o imagine</h4>
                    <button type="button" class="close text-light" data-dismiss="modal" id="modal-upload-image-close">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="border-color: rgba(0, 0, 0, 0.5)">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="modal-upload-image-file">
                        <label class="custom-file-label" name="modal-upload-img">Alegeți</label>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer" style="border-color: rgba(0, 0, 0, 0.5)">
                    <input type="submit" class="btn btn-block btn-success" value="Ok">
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function modalUploadImage(callback) {
        var url = "/api/uploadImage";
        var file_data = $('#modal-upload-image-file').prop('files')[0];   
        var form_data = new FormData($("#modal-upload-image-form")[0]);                  
        form_data.append('file', file_data);

        $.ajax({
            url: url,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        callback.call({}, resp.body.filename)
                        $("#modal-upload-image-close").trigger("click");
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            }
        });
    }

</script>