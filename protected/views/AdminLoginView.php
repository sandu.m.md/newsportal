<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin login</title>
    <?php Loader::load("Requires");?>
    <style>
        html, head, body {
            padding: 0;
            margin: 0;
            min-height: 100vh;
            min-width: 100vw;
            height: 100%;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-dark h-100 text-light">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="w-100 text-center pt-5" style="margin-top: 100px">
                            <form action="/admin/auth" method="post">
                                <h1 class="font-weight-light mb-5">Autentificare</h1>
                                <input type="text" class="form-control mt-4" style="border-radius: 2px 2px 0 0" name="username" placeholder="Numele de utilizător" required>
                                <input type="password" class="form-control" style="border-radius: 0 0 2px 2px" name="password" placeholder="Parola" required>
                                <input type="submit" class="btn btn-lg btn-block btn-success mt-4" value="Conectare">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</body>
</html>