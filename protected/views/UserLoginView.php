<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autentificare</title>
    <?php Loader::load("Requires")?>
</head>
<body class="bg-dark text-light">
    
    <div class="container-fluid pb-5">
        <div class="row bg-dark sticky-top p-2 px-3">
            <div class="col-md-* d-flex align-items-center justify-content-center">
                <a href="/" class=""><div class="cover-image mr-1" style="background-image: url(/res/ico/logo.png);height:2rem;width:2rem;"></div></a>
                <a href="/" class="btn p-0 text-light font-title">4you</a>
            </div>
        </div>

        <div class="row pt-5 mt-5">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form id="login-form" action="" onsubmit="return login()" class="pt-4">
                    <h1 class="font-weight-light mb-4 text-center">Autentificare</h1>
                    <input id="login-username" type="text" class="form-control form-control-lg mt-4" style="border-radius: 2px 2px 0 0" name="username" placeholder="Email" required>
                    <input id="login-password" type="password" class="form-control form-control-lg" style="border-radius: 0 0 2px 2px" name="password" placeholder="Parola" required>
                    <button id="login-submit" type="submit" class="btn btn-lg btn-block btn-primary mt-4">Connectare</button>
                    <div class="text-center pt-4">
                        <span>Nu ai un cont?</span>
                        <a href="/user/register" class="btn btn-sm btn-secondary">Înregistrează-te</a>
                    </div>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    <div class="pt-3"></div>
    <?php Loader::load("Footer", array(
        "feedback" => true
    ));?>

    <script>

        $("#login-username").on("input", function(e){
            $("#login-submit").html("Conectare").removeClass("btn-danger").addClass("btn-primary");
        });

        $("#login-password").on("input", function(e){
            $("#login-submit").html("Conectare").removeClass("btn-danger").addClass("btn-primary");
        });


        function login() {
            $("#login-submit").html('<div class="spinner-border"></div>');

            setTimeout(function(){
            

            $.post("/user/auth", $("#login-form").serializeArray(), function(data) {
                try {
                    var resp = JSON.parse(data);

                    // console.log(resp);

                    if (resp.ok) {
                        $("#login-submit").removeClass("btn-primary").addClass("btn-success");

                        setTimeout(function(){
                            console.log("Redirect to home");
                            window.location.href = "/user/home";
                        }, 1000);
                    } else {
                        $("#login-submit").html(resp.body.error).removeClass("btn-primary").addClass("btn-danger");
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            })

            }, 1000);

            return false;
        };
    
    </script>
</body>
</html>