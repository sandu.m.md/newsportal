<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz4U - Problems</title>

    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
        }
    
        body {
            overflow-x: hidden;
        }

        .btn-text {
            transition: 0.4s;
            cursor: pointer;
        }

        .btn-text:hover {
            color: #d3d3d3;
        }

        .btn-flex {
            transition: 0.4s;
            margin: 0;
            cursor: pointer;
            white-space: nowrap;
            border-bottom: 1px solid rgba(0, 0, 0, 0.08);
        }

        .btn-flex:hover {
            background-color: rgba(0, 0, 0, 0.2);
            color: #f8f9fa!important;
        }

        .btn-text-light {
            transition: 0.4s;
            text-decoration: none;
            color: #6c757d!important;
        }


        .btn-text-light:hover {
            color: #f8f9fa!important;
        }

        .shadow-light {
            transition: 0.4s;
            cursor: pointer;
            box-shadow: 3px 3px 15px rgba(0,0,0,0.2);
        }

        .shadow-light:hover {
            transform: translateY(-5px);
            box-shadow: 8px 8px 20px rgba(0,0,0,0.4);
            color: #f8f9fa!important;
        }

        .smooth {
            transition: 0.4s;
        }

        .bg-light-1 {
            background-color: rgba(255, 255, 255, 0.075);
        }
        .textarea{
            /* background-color: rgba(255, 255, 255, 0.075); */

            margin-top: 2%;
            transition: 1.5s;
            color:black;    
        }
    </style>
</head>
<body class="bg-dark text-light">

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top pt-1 pb-1" id="nav">
            <button class="btn text-secondary btn-text-light" onclick="window.location.href='/quiz'">
                <i class="fa fa-chevron-left"></i>
                <span>Înapoi</span>
            </button>
            <?php if (App::$user->isAdmin()) { ?>
                <div class="col-md-* p-1 pr-2 ml-auto">
                </div>
            <?php } ?>
            <?php if(App::$conf->user->notifications->enabled && false) { ?>
                <div class="col-md-* p-1 pr-3 <?php if(!App::$user->isAdmin())echo"ml-auto"?>">
                    <button class="btn btn-lg p-1 hover-text-warning <?php if(App::$user->hasNotifications())echo"text-warning";else echo"text-light";?>" onclick="window.location.href='/user/notifications'">
                        <i class="far fa-bell position-relative"> 
                            <?php if(App::$user->hasNotifications()) { ?>
                                <i class="fa fa-circle text-danger position-absolute bordder bordder-dark" style="font-size:9px; top:0px; right:0px;"></i>
                            <?php } ?>
                        </i>
                    </button>
                </div>
            <?php } ?>
        </div>

        <div class="container p-0">
            <div class="row pt-4">
                <div class="col-md-3"></div>
                
                <!-- Content -->
                <div class="col-md-6 mb-5 pb-1">
                    <div class="mt-3 pb-3">
                        <h2 class="font-weight-light text-center">Vă mulțumim pentru că ați ales să ne trimiteți un feedback!</h2>
                        <h6 class="font-weight-light text-center text-secondary m-0">
                            <i class="fa fa-info-circle"></i>
                            <span>Lăsați un mesaj în căsuța de mai jos, iar noi vom reveni cu un răspuns cât de curând posibil.</span>
                        </h6>
                    </div>
                    <form action="/help/catchProblem" method="POST">
                        <div class="form-group">
                            <label for="message">Mesaj</label>
                            <textarea id="message" class="form-control" id="exampleFormControlTextarea1" rows="3" name="text" placeholder="Introduceți textul aici"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input id="email" type="text" class="form-control" name="email" placeholder="example@gmail.com">
                        </div>
                        
                        <div class="text-center">
                            <button class="btn btn-lg text-secondary btn-text-light">Trimite feedback <i class="fa fa-arrow-right"></i></button>
                        </div>
                    </form>
                </div>

                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
    
    <?php Loader::load("Footer", array(
        "user" => true
    ))?>

</body>                   
                    
</html>