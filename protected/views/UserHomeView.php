<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TopTest - Acasă</title>

    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
        }
    
        body {
            overflow-x: hidden;
        }

        .btn-text {
            transition: 0.4s;
            cursor: pointer;
        }

        .btn-text:hover {
            color: #d3d3d3;
        }

        .btn-flex {
            transition: 0.4s;
            margin: 0;
            cursor: pointer;
            white-space: nowrap;
            border-bottom: 1px solid rgba(0, 0, 0, 0.08);
        }

        .btn-flex:hover {
            background-color: rgba(0, 0, 0, 0.2);
            color: #f8f9fa!important;
        }

        .btn-text-light {
            transition: 0.4s;
            text-decoration: none;
            color: #6c757d!important;
        }


        .btn-text-light:hover {
            color: #f8f9fa!important;
        }

        .smooth {
            transition: 0.4s;
        }
    </style>
    <script>
        function addQuizHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[hidden-info]").css("opacity", "1");
                $(selector).children("[q-from]").addClass("text-info");
            }, function(){
                $(selector).children("[hidden-info]").css("opacity", "0");
                $(selector).children("[q-from]").removeClass("text-info");
            });
        }
    </script>
</head>
<body class="bg-dark text-light" data-spy="scroll" data-target=".navbar" data-offset="400">

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top p-2 px-3" id="nav">
            <div class="col-md-* d-flex align-items-center justify-content-center">
                <a href="/"><div class="cover-image" style="background-image: url(/res/ico/logo.png);height:2rem;width:2rem;"></div></a>
            </div>
            <?php if(App::$conf->user->notifications->enabled) { ?>
                <div class="col-md-* ml-auto">
                    <button class="btn btn-dark p-1 px-2 btn-lg hover-text-warning <?php if(App::$user->hasNotifications())echo"text-warning";else echo"text-light";?>" onclick="window.location.href='/user/notifications'">
                        <i class="far fa-bell position-relative"> 
                            <?php if(App::$user->hasNotifications()) { ?>
                                <i class="fa fa-circle text-danger position-absolute bordder bordder-dark" style="font-size:9px; top:0px; right:0px;"></i>
                            <?php } ?>
                        </i>
                    </button>
                </div>
            <?php } ?>
        </div>

        <div class="container p-0">
            <div class="row pt-4">
                <div class="col-md-3"></div>

                <div class="col-md-6 mb-5 pb-1 smooth">
                    <div>
                        <div class="display-3 text-center">
                            <i class="far fa-user"></i>
                        </div>
                        <div class="display-4 text-center">
                            <?php echo App::$user->nick?>
                        </div>

                        <div class="d-flex mt-5">
                            <div class="flex-fill text-center p-1 pt-3 pb-3">
                                <div class="d-flex flex-column">
                                    <h5 class="font-weight-light text-success mb-0">
                                        <i class="fas fa-check-double mr-1"></i>
                                        <span><?php echo App::$user->rating()["passed_count"]?></span>
                                    </h5>
                                    <h6 class="font-weight-light text-secondary">Rezolvări</h6>
                                </div>
                            </div>
                            <div class="flex-fill text-center p-1 pt-3 pb-3">
                                <div class="d-flex flex-column" title="Rezolvați teste pentru a vă ridica poziția!">
                                    <h2 class="font-weight-light text-warning mb-0">
                                        <i class="fas fa-award mr-1"></i>
                                        <span>Locul <?php echo App::$user->rating()["place"];?></span>
                                    </h2>
                                    <h6 class="font-weight-light text-secondary">Din <?php echo App::$user->rating()["total"];?></h6>
                                </div>
                            </div>
                            <div class="flex-fill text-center p-1 pt-3 pb-3">
                                <div class="d-flex flex-column">
                                    <h5 class="font-weight-light text-info mb-0">
                                        <i class="fa fa-trophy mr-1"></i>
                                        <span><?php echo (int)App::$user->rating()["rating"]?></span>
                                    </h5>
                                    <h6 class="font-weight-light text-secondary">Puncte</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 p-0 mb-5">
                    <?php if (App::$user->isManager()) { ?>
                        <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex" onclick="window.location.href='/user/quizzes'">
                            <i class="fas fa-list mr-1"></i>
                            <span>Testele mele</span>
                        </h4>
                    <?php } else { ?>
                        <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex" data-toggle="modal" data-target="#modal-question-edit">
                            <i class="far fa-lightbulb pl-1 mr-2"></i>
                            <span>Sugerează o întrebare</span>
                        </h4>
                    <?php } ?>
                    <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex" onclick="window.location.href='/user/top'">
                        <i class="fas fa-trophy mr-1"></i>
                        <span>Top jucători</span>
                    </h4>
                    <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex" onclick="window.location.href='/user/results'">
                        <i class="fas fa-star mr-1"></i>
                        <span>Realizările mele</span>
                    </h4>
                    <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex">
                        <i class="fas fa-cog mr-1"></i>
                        <span>Setări</span>
                    </h4>
                    <h4 class="font-weight-light text-secondary p-3 pl-4 btn-flex" onclick="window.location.href='/user/logout'">
                        <i class="fas fa-sign-out-alt mr-1"></i>
                        <span>Ieșire</span>
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="" class="pt-5">
            <?php Loader::Load("QuizCardSetC", array(
                "title" => "Recomandări",
                "quizzes" => $data->recommended,
                "container" => "#recommended",
                "showed" => 3
            ))?>
        </div>
    </div>

    <?php Loader::load("Footer", array(
        "feedback" => true
    ))?>

    <?php Loader::load("ModalQuestionEdit")?>

    <script>
        function quizClick(target) {    
            var info = $(target).children(".marker");

            if (info.hasClass("d-none")) {
                info.removeClass("d-none");
            } else {
                info.addClass("d-none");
            }
        }

        $(window).on('activate.bs.scrollspy', function (e) {
            var selector = $(".nav-link.active").attr("href");

            $("[spy]").css("opacity", "0.3");
            $(selector).css("opacity", "1");

            // $("[spy]").addClass("text-secondary").removeClass("text-light");
            // $(selector).removeClass("text-secondary").addClass("text-light");
        });
    </script>
</body>
</html>