<?php if (isset($data->quiz)) { $quiz = &$data->quiz;?>
    <div class="col-md-4 p-3 <?php if(!$data->extended && $data->hidden) echo"d-none"?>" <?php if($data->hidden) echo "d-none"?> id="<?php echo $data->idPrefix.$quiz->id?>">
        <?php if (isset($data->backAnchor) && $data->backAnchor) {?>
            <span id="<?php echo substr($data->backAnchor, 1)?>-anchor"></span>
        <?php } ?>
        <div class="rounded-xl h-100">
            <div class="d-flex h-100 flex-column">
                <a href="/quiz/view/<?php echo $quiz->id?>" id="<?php echo $data->idPrefix.$quiz->id?>-hidden-content" class="flex-fill cover-image rounded-xl position-relative shadow" style="height:11rem;background-image:url(<?php echo $quiz->cover?>);">
                    <div class="position-absolute  cover cover-fadeout w-100 h-100" style="left:0;top:0;background-color:#00000033"></div>
                    <div hidden-info class="position-absolute smooth text-center text-light w-100" style="top:0;left:0px;opacity:0;font-size:13px;transition:0.5s;text-shadow:2px 2px 6px #00000088">
                        <h5 style="display:inline"><span class="badge badge-dark"><i class="fa fa-user mr-1"></i><?php echo $quiz->passed?></span></h5>
                        <h5 style="display:inline"><span class="badge badge-dark"><i class="far fa-eye mr-1"></i><?php echo $quiz->views?></span></h5>
                        <?php if (isset($quiz->unique_medium_duration) && $quiz->unique_medium_duration) { ?>
                            <h5 style="display:inline"><span class="badge badge-dark"><i class="far fa-clock mr-1"></i><?php echo explode(".", $quiz->unique_medium_duration)[0]?>s</span></h5>
                        <?php } ?>
                    </div>
                </a>
                <div class="flex-fill pt-2 d-flex">
                    <div class="flex-fill">
                        <a href="/quiz/view/<?php echo $quiz->id?>" class="flex-fill <?php echo App::$theme->text->color_alt." ".App::$theme->text->hover_alt?>" style="text-decoration: none"><?php echo $quiz->name?></a>
                    </div>
                    <!-- <div class="">
                        <a href="/quiz/view/<?php echo $quiz->id?>" class="btn btn-outline-primary" style="display:block;white-space:nowrap"><i class="fa fa-play mr-1"></i>Play</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <script>
        addQuizHover('<?php echo "#".$data->idPrefix.$quiz->id?>-hidden-content');
    </script>
<?php } ?>