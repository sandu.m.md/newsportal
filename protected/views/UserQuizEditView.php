<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TopTest - Acasă</title>

    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
        }
    
        body {
            overflow-x: hidden;
        }

        .btn-text {
            transition: 0.4s;
            cursor: pointer;
        }

        .btn-text:hover {
            color: #d3d3d3;
        }

        .btn-flex {
            transition: 0.4s;
            margin: 0;
            cursor: pointer;
            white-space: nowrap;
            border-bottom: 1px solid rgba(0, 0, 0, 0.08);
        }

        .btn-flex:hover {
            background-color: rgba(0, 0, 0, 0.2);
            color: #f8f9fa!important;
        }

        .btn-text-light {
            transition: 0.4s;
            text-decoration: none;
            /* color: #6c757d!important; */
        }

        .smooth {
            transition: 0.1s;
        }

        .hover-text-light:hover {
            color: #f8f9fa!important;
        }


        .btn-text-light:hover {
            color: #f8f9fa!important;
        }


    </style>
</head>
<body class="bg-dark text-light" data-spy="scrol" data-target=".navbar" data-offset="200">

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top d-none">  
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="#spy-1"></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#spy-2"></a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top pt-1 pb-1" id="nav">
            <div class="col-md-* p-1 pr-1">
                <button class="btn text-light hover-text-primary" onclick="window.location.href='/user/quizzes'">
                    <i class="fa fa-chevron-left"></i>
                    <span>Teste</span>
                </button>
            </div>
            <div class="col-md-* p-1 d-flex align-items-center ml-auto">
                <input type="checkbox" <?php echo isset($data->quiz)&&$data->quiz->active?"checked":"";?> data-toggle="toggle" data-on="Activ" data-off="Inactiv" data-size="sm" data-style="ios" <?php echo isset($data->quiz)?:'disabled'?> onchange="changeQuizStatus(this, <?php echo isset($data->quiz)?$data->quiz->id:0?>)">
            </div>
            <div class="col-md-* p-1 d-flex align-items-center">
                <div class="btn bnt-lg text-light hover-text-success" onclick="window.location.href='/quiz/view/<?php echo$data->quiz->id?>'">
                    <i class="fa fa-play"></i>    
                </div>
            </div>
            <div class="col-md-* p-1 d-flex align-items-center">
                <div class="btn bnt-lg pl-0 text-light hover-text-primary" data-toggle="modal" data-target="#modal-question-edit" onclick="clearModalQuestionEdit(parseInt(<?php echo$data->quiz->id;?>));">
                    <i class="fa fa-plus"></i>    
                </div>
            </div>
        </div>

        <div class="container-fluid p-0">
            <div class="row pt-4">
                <div class="col-md-3 mb-5" style="border-right: 1px solid rgba(255,255,255,0.1)">
                    <div class="position-relative pt-4">
                        <h3 class="font-weight-light text-center m-0 px-4">
                            <span id="quiz-title"><?php echo isset($data->quiz) ? $data->quiz->name : "Click pentru a adăuga un titlu" ?></span>
                        </h3>
                        <div class="position-absolute text-secondary hover-text-light" style="left:0px;top:0px;">Titlu</div>
                        <div class="btn btn-sm position-absolute text-secondary hover-text-light" style="right:0px;top:0px;" onclick="enableDisableEdit('#quiz-title')">
                            <i class="fa fa-pen"></i>
                        </div>
                    </div>
                    <div class="position-relative mt-4 pt-4">
                        <div class="position-relative mt-1">
                            <img class="img-fluid rounded" id="quiz-cover" src="<?php echo isset($data->quiz->cover)&&$data->quiz->cover ? $data->quiz->cover:'/res/images/2019_12_15_00_55_45_784027.jpg'?>">
                            <div class="btn btn-sm btn-primary position-absolute" style="right:5px;top:5px;" data-toggle="modal" data-target="#modal-upload-image" onclick="">
                                <i class="fa fa-pen"></i>
                            </div>
                        </div>    
                        <div class="position-absolute text-secondary hover-text-light" style="left:0px;top:0px;">Copertă</div>
                    </div>
                </div>

                <div class="col-md-6 mb-5" style="border-right: 1px solid rgba(255,255,255,0.1)">
                    <h2 class="font-weight-light text-center">Întrebări</h2>
                    <div>
                        <?php if (isset($data->questions)) foreach ($data->questions as $question) { ?>
                            <div class="text-light hover-text-primary position-relative shadow-box bg-lighter rounded mt-3">
                                <input type="hidden" id="question-<?php echo$question->id?>-quiz-id" value="<?php echo $question->quiz_id?>">
                                <input type="hidden" id="question-<?php echo$question->id?>-question" value="<?php echo $question->question?>">
                                <input type="hidden" id="question-<?php echo$question->id?>-answer-1" value="<?php echo $question->answer_1?>">
                                <input type="hidden" id="question-<?php echo$question->id?>-answer-2" value="<?php echo $question->answer_2?>">
                                <input type="hidden" id="question-<?php echo$question->id?>-answer-3" value="<?php echo $question->answer_3?>">
                                <input type="hidden" id="question-<?php echo$question->id?>-correct-answer" value="<?php echo $question->correct_answer?>">
                                <h6 class="font-weight-light px-3 pt-4 pb-4">
                                    <span><?php echo$question->question?></span>
                                </h6>
                                <div class="btn btn-sm position-absolute btn-text-light text-secondary" style="right:0px;top:0px;" data-toggle="modal" data-target="#modal-question-edit" onclick="fillModalQuestionEdit(parseInt(<?php echo $question->id?>))">
                                    <i class="fa fa-pen"></i>
                                </div>
                            </div>
                        <? } ?>
                    </div>
                </div>

                <div class="col-md-3 mb-5">
                    <h4 class="font-weight-light">Scara de apreciere</h4>
                    <?php if ($data->quiz->rating_scale_id) echo $data->quiz->rating_scale_id ?>
                </div>
            </div>
        </div>    
    </div>

    <!-- Modal Question Edit -->
    <?php Loader::load("ModalQuestionEdit");?>

    <!-- Modal Upload Image -->
    <?php Loader::load("ModalUploadImage");?>
    <script>
        $("#modal-upload-image-form").submit(function(){
            modalUploadImage(function(filename){
                $("#quiz-cover").attr("src", filename);
                saveQuiz();
            });
        });
    </script>


    <script>
        var quizId = parseInt(<?php echo isset($data->quiz)?$data->quiz->id:0?>);

        function saveQuiz() {
            $.post("/user/saveQuiz<?php echo isset($data->quiz)?"/".$data->quiz->id:''?>", {title: $("#quiz-title").html(), cover: $("#quiz-cover").attr("src")}, function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (resp.body !== false && resp.body !== true) {
                            window.location.replace("/user/quizEdit/"+resp.body.id);
                        }
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        }

        $("#quiz-title").focusout(function(){
            saveQuiz();
        })

        function enableDisableEdit(selector) {
            $(selector).focusout(function(e){
                $(e.target).attr("contenteditable", "false");
            });

            $(selector).attr("contenteditable", "true");
            $(selector).focus();
        }
        
        $(window).on('activate.bs.scrollspy', function (e) {
            var selector = $(".nav-link.active").attr("href");

            $("[spy]").addClass("text-secondary").removeClass("text-light");
            $(selector).removeClass("text-secondary").addClass("text-light");
        });
        
        function changeQuizStatus(target, id){
            var url = "/quiz/enable/";
            if ($(target).attr("checked")) {
                url = "/quiz/disable/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

    </script>

</body>
</html>