<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TopTest - Acasă</title>

    <?php Loader::load("Requires")?>
    <script>
        function addQuizHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[hidden-info]").css("opacity", "1");
                $(selector).children("[q-from]").addClass("text-info");
            }, function(){
                $(selector).children("[hidden-info]").css("opacity", "0");
                $(selector).children("[q-from]").removeClass("text-info");
            });
        }

        function shrinkQuizzes(target, containerSelector) {
            $(containerSelector).children('[d-none]').addClass('d-none');
            $(target).addClass("d-none");
            $(target).next().removeClass("d-none");
        }

        function extendQuizzes(target, containerSelector) {
            $(containerSelector).children('[d-none]').removeClass('d-none');
            $(target).addClass("d-none");
            $(target).prev().removeClass("d-none");
        }
    </script>
</head>
<body class="bg-dark text-light">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            
            <div class="col-md-6">
                
                <div id="recommended" class="mt-5">
                    <?php Loader::load("QuizCardSetC", array(
                        "title" => "Recomandate",
                        "quizzes" => QuizModel::getRecommended(),
                        "container" => "#recommended",
                    ))?>
                </div>

                <div id="newest" class="mt-5">
                    <?php Loader::load("QuizCardSetC", array(
                        "title" => "Cele mai noi",
                        "quizzes" => QuizModel::getNewest(),
                        "container" => "#newest",
                    ))?>
                </div>

                <div id="top" class="mt-5">
                    <?php Loader::load("QuizCardSetC", array(
                        "title" => "Cele mai interesante",
                        "quizzes" => QuizModel::getTop(),
                        "container" => "#top",
                    ))?>
                </div>

            </div>

            <div class="col-md-3"></div>
        </div>
    </div>

</body>
</html>