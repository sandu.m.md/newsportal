<?php if (isset($data->quiz)) { $quiz = &$data->quiz;?>
    <div id="quiz-preview" class="row" style="min-height: 100vh">
        <div class="col-md-12 cover cover-image" style="height:27vh;background-image:url(/res/wave_quiz_2.png);background-position:bottom">
            <div class="row p-2">
                <div class="col-md-*">
                    <a href="/quiz" class="btn text-light hover-bg-darker"><i class="fa fa-home mr-1"></i>Home</a>
                </div>
                <h2 class="col-md-5 text-center m-0 mt-3 font-weight-light mx-auto"><?php echo $data->quiz->name?></h2>
                <div class="col-md-*">
                    <?php if (App::$user->isLoggedIn()) { ?>
                        <a href="/user/home" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                    <?php } else { ?>
                        <a href="/user/login" class="btn text-light hover-bg-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="mx-auto position-relative shadow-lg">
                        <img src="<?php echo $data->quiz->cover?>" class="img-fluid invisible" alt="">
                        <div class="cover cover-image w-100 h-100 rounded position-absolute" style="background-image:url(<?php echo $data->quiz->cover?>);top:0;left:0;"></div>
                        <div class="cover cover-daarker cover-fadeout rounded position-absolute w-100 h-100" style="top:0;left:0;"></div>
                        <h2 class="font-weight-light position-absolute d-flex justify-content-center align-items-center h-100 px-5 w-100" style="left:0;top:0;">
                            <!-- <div class="text-center"><?php echo $data->quiz->name?></div> -->
                        </h2>
                    </div>
                    
                    <div class="mx-auto mt-3 d-flex">
                        <div class="flex-fill d-flex justify-content-center align-items-center">
                            <div class="shadow position-relative d-flex justify-content-center align-items-center bg-special text-dark zoom hover-text-danger" style="border-radius:50%;height:3rem;width:3rem" title="Numărul de întrebări">
                                <h4 class="m-0 text-light"><?php echo $data->quiz->questions_count;?></h4>
                                <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.25;"><i class="fa fa-question text-warning"></i></h1>                                
                            </div>
                        </div>
                        <div class="flex-fill d-flex justify-content-center align-items-center">
                            <div class="shadow position-relative d-flex justify-content-center align-items-center bg-ico-1 text-dark zoom hover-text-danger" style="border-radius:50%;height:3rem;width:3rem;overflow:hidden;" title="Numărul de jucători care au trecut testul">
                                <h4 class="m-0 text-light"><?php echo $data->quiz->passed;?></h4>
                                <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.25;"><i class="fa fa-user text-light"></i></h1>                                
                            </div>
                        </div>
                        <div class="flex-fill d-flex justify-content-center align-items-center">
                            <div class="shadow position-relative d-flex justify-content-center align-items-center bg-ico-2 text-dark zoom hover-text-danger" style="border-radius:50%;height:3rem;width:3rem" title="Timpul mediu în care a fost trecut testul(în secunde)">
                                <h4 class="m-0 text-light"><?php echo (int)$data->quiz->unique_medium_duration;?></h4>
                                <h1 class="m-0 position-absolute w-100 h-100 d-flex justify-content-center align-items-center" style="left:0;top:0;opacity:0.30;"><i class="fa fa-clock text-light"></i></h1>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>

        <div class="col-md-12">
            <div class="row">
                
            </div>
        </div>
        
        <div class="col-md-12"></div>

        <div class="col-md-12 text-center">
            <div class="text-center w-100">
                <div id="btn-quiz-start" class="btn btn-danger btn-lg state-1" style="transition: all 1s;transform:scale(3);opacity:0" onclick="start_quiz();"><i class="fa fa-play"></i> Începe testul acum!</div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="position-absolutes w-100 text-center" style="bottom:9rem;left:0">
                <a href="#next-quizzes" onclick="highlightChildren('#recommended')">
                    <div class="btn text-secondary hover-text-danger" id="preview-scroll-down">
                        <i class="fa fa-angle-double-down display-4"></i>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-12"></div>
    </div>

<?php } ?>