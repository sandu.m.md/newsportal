<?php if (isset($data->quiz)) { $quiz = &$data->quiz;?>
    <div class="position-relative rounded cover-image border border-secondary shadow-light mt-4 <?php if(!$data->extended && $data->hidden) echo"d-none"?>" <?php if($data->hidden) echo "d-none"?> id="<?php echo $data->idPrefix.$quiz->id?>">
        <div class="d-flex">
            <div class="flex-fill w-50 rounded-left text-center p-3 bg-darkk" style="backgrbound: #007bff">
                <div class="rounded-right">
                    <h5 class="font-weight-light">
                        <span><?php echo$quiz->name?></span>
                    </h5>
                </div>
                <div class="text-center w-100">
                    <a href="/quiz/view/<?php echo$quiz->id?>" class="btn btn-danger">
                        <i class="fa fa-play"></i>
                        <span>Joacă acum</span>
                    </a>
                </div>
            </div>
            <div class="flex-fill w-50 rounded-right cover-image" style="background-image:url(<?php echo $quiz->cover?>);">
                <div class="cover cover-fade-to-rightt w-50"></div>
            </div>
        </div>
        
        <div hidden-info class="position-absolute smooth text-center text-secondary w-100" style="bottom:-18px;left:0px;opacity:0;font-size:13px;transition:0.5s">
            <span><i class="far fa-calendar-alt mr-1"></i><?php echo date("j F, Y H:i", strtotime($quiz->datetime_created))?></span>
            <span class="ml-4"><i class="fa fa-question-circle mr-1"></i><?php echo $quiz->questions_count?> întrebări</span>
        </div>
    </div>
    <script>
        addQuizHover('<?php echo "#".$data->idPrefix.$quiz->id?>');
    </script>
<?php } ?>