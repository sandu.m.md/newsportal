<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php Loader::load("Requires");?>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-ajax-downloader@1.1.0/src/ajaxdownloader.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row p-2 bg-dark text-light sticky-top">
            <div class="col-md-*">
                <a href="/admin" class="btn btn-sm btn-success">Acasă</a>
            </div>
            <div class="col-md-* ml-2">
                <!-- <a href="/quiz/edit" class="btn btn-sm btn-success">Test nou</a> -->
            </div>
            <div class="col-md-* ml-auto">
                <a href="/admin/logout" class="btn btn-sm btn-secondary">Log out</a>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-1" id="tab-1-link" onclick="activeTab=1;">Jucători</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-2" id="tab-2-link" onclick="activeTab=2;">Sesiuni</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-3" id="tab-2-link" onclick="activeTab=2;">Azi</a>
                    </li>
                    <li class="nav-item">
                        <!-- <a class="nav-link" data-toggle="tab" href="#posts-removed">Postări șterse</a> -->
                    </li>
                </ul>

                <div class="tab-content mt-4">
                    <!-- Tab 1 -->
                    <div id="tab-1" class="container-fluid tab-pane">
                        <div class="row">
                            <div class="col-md-3">
                                <h1 class="font-weight-light"><span class="badge badge-success">Online</span></h1>
                                <h5 class="font-weight-light" id="tab-1-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-1-viewers"></h5>
                                <h1 class="font-weight-light"><span class="badge badge-info">Statistici</span></h1>
                                <h5 class="font-weight-light" id="tab-1-unique-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-1-started"></h5>
                                <h5 class="font-weight-light" id="tab-1-finished"></h5>
                                <h5 class="font-weight-light" id="tab-1-views"></h5>
                                <h5 class="font-weight-light" id="tab-1-rating"></h5>
                                <h5 class="font-weight-light" id="tab-1-answers"></h5>
                                <h5 class="font-weight-light" id="tab-1-correct-answers"></h5>
                                <h5 class="font-weight-light" id="tab-1-skipped-answers"></h5>
                            </div>
                        
                            <div class="col-md-9">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Timp</th>
                                            <th>Începute</th>
                                            <th>Finisate</th>
                                            <th>Răspunsuri</th>
                                            <th>Rezultat</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tab-1-table">
                                        <tr>
                                            <td colspan="6" class="text-center">
                                                <span class="spinner-grow text-primary"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Tab 2 -->
                    <div id="tab-2" class="container-fluid tab-pane fade">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <div class="input-group">
                                    <input type="datetime-local" class="form-control">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary">Încarcă</button>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h1 class="font-weight-light"><span class="badge badge-success">Online</span></h1>
                                <h5 class="font-weight-light" id="tab-2-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-2-viewers"></h5>
                                <h1 class="font-weight-light"><span class="badge badge-info">Statistici</span></h1>
                                <h5 class="font-weight-light" id="tab-2-unique-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-2-started"></h5>
                                <h5 class="font-weight-light" id="tab-2-finished"></h5>
                                <h5 class="font-weight-light" id="tab-2-views"></h5>
                                <h5 class="font-weight-light" id="tab-2-rating"></h5>
                                <h5 class="font-weight-light" id="tab-2-answers"></h5>
                                <h5 class="font-weight-light" id="tab-2-correct-answers"></h5>
                                <h5 class="font-weight-light" id="tab-2-skipped-answers"></h5>
                            </div>
                        
                            <div class="col-md-9">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Jucător</th>
                                            <th>Test</th>
                                            <th>Start</th>
                                            <th>Stop</th>
                                            <th>Răspunsuri</th>
                                            <th>Corecte</th>
                                            <th>Rezultat</th>
                                            <th>Finisat</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tab-2-table">
                                        <tr>
                                            <td colspan="6" class="text-center">
                                                <span class="spinner-grow text-primary"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!-- Tab 3 -->
                    <div id="tab-3" class="container-fluid tab-pane fade">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <input type="datetime-local" class="form-control">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary">Încarcă</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <h1 class="font-weight-light"><span class="badge badge-success">Online</span></h1>
                                <h5 class="font-weight-light" id="tab-2-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-2-viewers"></h5>
                                <h1 class="font-weight-light"><span class="badge badge-info">Statistici</span></h1>
                                <h5 class="font-weight-light" id="tab-2-unique-players"><div class="spinner-grow text-primary"></div></h5>
                                <h5 class="font-weight-light" id="tab-2-started"></h5>
                                <h5 class="font-weight-light" id="tab-2-finished"></h5>
                                <h5 class="font-weight-light" id="tab-2-views"></h5>
                                <h5 class="font-weight-light" id="tab-2-rating"></h5>
                                <h5 class="font-weight-light" id="tab-2-answers"></h5>
                                <h5 class="font-weight-light" id="tab-2-correct-answers"></h5>
                                <h5 class="font-weight-light" id="tab-2-skipped-answers"></h5>
                            </div>
                        
                            <div class="col-md-9">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Timp</th>
                                            <th>Începute</th>
                                            <th>Finisate</th>
                                            <th>Răspunsuri</th>
                                            <th>Rezultat</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tab-2-table">
                                        <tr>
                                            <td colspan="6" class="text-center">
                                                <span class="spinner-grow text-primary"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var activeTab = 1;
        var renderInterval = 1; // seconds

        $("#tab-"+activeTab+"-link").trigger("click");

        setInterval(function(){

            switch(activeTab) {
                case 1:
                    tabRender1();
                    break;
             
                case 2:
                    tabRender2();
                    break;
            }
        }, renderInterval*1000);

        function tabRender1() {
            $.post("/quiz/getStatistics", {}, function(data){
                try{
                    var resp = JSON.parse(data);
                    var table = $("#tab-1-table");

                    if (resp.ok) {
                        table.empty();

                        $.each(resp.body.players, function(key, value){
                            var html = "";
                            html += '<tr>';

                            html += '<td>'+value.id+'</td>';
                            html += '<td>'+value.datetime+'</td>';
                            html += '<td>'+value.quiz_cnt_started+'</td>';
                            html += '<td>'+value.quiz_cnt_passed+'</td>';
                            html += '<td>'+value.questions_cnt+'</td>';
                            html += '<td>'+value.rating.slice(0, 5)+'%</td>';

                            html += '</tr>';

                            table.append(html);
                        });

                        $('#tab-1-started').empty().append("Teste începute: "+resp.body.stats.started);
                        $('#tab-1-finished').empty().append("Teste finisate: "+resp.body.stats.finished);
                        $('#tab-1-views').empty().append("Vizualizări: "+resp.body.stats.views);
                        $('#tab-1-players').empty().append("Jucători: "+resp.body.stats.online_players);
                        $('#tab-1-viewers').empty().append("Vizualizatori: "+(parseInt(resp.body.stats.online_viewers)));
                        $('#tab-1-rating').empty().append("Rezultat comun: "+resp.body.stats.rating.slice(0, 5)+"%");
                        $('#tab-1-answers').empty().append("Total răspunsuri: "+resp.body.stats.total_answers);
                        $('#tab-1-correct-answers').empty().append("Răspunsuri corecte: "+resp.body.stats.correct_answers);
                        $('#tab-1-skipped-answers').empty().append("Întrebări omise: "+resp.body.stats.skipped_answers);
                        $('#tab-1-unique-players').empty().append("Jucători unici: "+resp.body.stats.unique_players);
                    } else {
                        console.log("Bad");
                    }
                } catch(ex) {
                    console.log(ex);
                }
            });
        }

        function tabRender2() {
            $.post("/quiz/getSessions", {}, function(data){
                try{
                    var resp = JSON.parse(data);
                    var table = $("#tab-2-table");

                    if (resp.ok) {
                        table.empty();
                        // console.log(resp);

                        $.each(resp.body.sessions, function(key, value){
                            var html = "";
                            html += '<tr>';

                            html += '<td>'+value.id+'</td>';
                            html += '<td>'+value.guest_id+'</td>';
                            html += '<td>'+value.quiz_name+'</td>';
                            html += '<td>'+value.datetime_start+'</td>';
                            html += '<td>'+value.datetime_stop+'</td>';
                            html += '<td>'+value.answers+'</td>';
                            html += '<td>'+value.correct_answers+'</td>';
                            html += '<td>'+value.correct_percentage.slice(0, 5)+'%</td>';
                            html += '<td>'+(value.passed==1?'Da':'Nu')+'</td>';

                            html += '</tr>';

                            table.append(html);
                        });

                        // $('#tab-2-started').empty().append("Teste începute: "+resp.body.stats.started);
                        // $('#tab-2-finished').empty().append("Teste finisate: "+resp.body.stats.finished);
                        // $('#tab-2-views').empty().append("Vizualizări: "+resp.body.stats.views);
                        // $('#tab-2-players').empty().append("Jucători: "+resp.body.stats.online_players);
                        // $('#tab-2-viewers').empty().append("Vizualizatori: "+(parseInt(resp.body.stats.online_players)-parseInt(resp.body.stats.online_viewers)));
                        // $('#tab-2-rating').empty().append("Rezultat comun: "+resp.body.stats.rating.slice(0, 5)+"%");
                        // $('#tab-2-answers').empty().append("Total răspunsuri: "+resp.body.stats.total_answers);
                        // $('#tab-2-correct-answers').empty().append("Răspunsuri corecte: "+resp.body.stats.correct_answers);
                        // $('#tab-2-skipped-answers').empty().append("Întrebări omise: "+resp.body.stats.skipped_answers);
                        // $('#tab-2-unique-players').empty().append("Jucători unici: "+resp.body.stats.unique_players);
                    } else {
                        console.log("Bad");
                    }
                } catch(ex) {
                    console.log(ex);
                }
            });
        }

        function tabRender3() {
            $.post("/quiz/getStatistics", {}, function(data){
                try{
                    var resp = JSON.parse(data);
                    var table = $("#tab-2-table");

                    if (resp.ok) {
                        table.empty();

                        $.each(resp.body.players, function(key, value){
                            var html = "";
                            html += '<tr>';

                            html += '<td>'+value.id+'</td>';
                            html += '<td>'+value.datetime+'</td>';
                            html += '<td>'+value.quiz_cnt_started+'</td>';
                            html += '<td>'+value.quiz_cnt_passed+'</td>';
                            html += '<td>'+value.questions_cnt+'</td>';
                            html += '<td>'+value.rating.slice(0, 5)+'%</td>';

                            html += '</tr>';

                            table.append(html);
                        });

                        $('#tab-2-started').empty().append("Teste începute: "+resp.body.stats.started);
                        $('#tab-2-finished').empty().append("Teste finisate: "+resp.body.stats.finished);
                        $('#tab-2-views').empty().append("Vizualizări: "+resp.body.stats.views);
                        $('#tab-2-players').empty().append("Jucători: "+resp.body.stats.online_players);
                        $('#tab-2-viewers').empty().append("Vizualizatori: "+(parseInt(resp.body.stats.online_players)-parseInt(resp.body.stats.online_viewers)));
                        $('#tab-2-rating').empty().append("Rezultat comun: "+resp.body.stats.rating.slice(0, 5)+"%");
                        $('#tab-2-answers').empty().append("Total răspunsuri: "+resp.body.stats.total_answers);
                        $('#tab-2-correct-answers').empty().append("Răspunsuri corecte: "+resp.body.stats.correct_answers);
                        $('#tab-2-skipped-answers').empty().append("Întrebări omise: "+resp.body.stats.skipped_answers);
                        $('#tab-2-unique-players').empty().append("Jucători unici: "+resp.body.stats.unique_players);
                    } else {
                        console.log("Bad");
                    }
                } catch(ex) {
                    console.log(ex);
                }
            });
        }

    </script>
</body>
</html>