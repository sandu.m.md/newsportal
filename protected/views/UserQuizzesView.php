<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TopTest - Testele mele</title>

    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
        }
    
        body {
            overflow-x: hidden;
        }

        .btn-text {
            transition: 0.4s;
            cursor: pointer;
        }

        .btn-text:hover {
            color: #d3d3d3;
        }

        .btn-flex {
            transition: 0.4s;
            margin: 0;
            cursor: pointer;
            white-space: nowrap;
            border-bottom: 1px solid rgba(0, 0, 0, 0.08);
        }

        .btn-flex:hover {
            background-color: rgba(0, 0, 0, 0.2);
            color: #f8f9fa!important;
        }

        .btn-text-light {
            transition: 0.4s;
            text-decoration: none;
            /* color: #6c757d!important; */
        }

        .smooth {
            transition: 0.1s;
        }

        .hover-text-light:hover {
            color: #f8f9fa!important;
        }


        .btn-text-light:hover {
            color: #f8f9fa!important;
        }


    </style>
</head>
<body class="bg-dark text-light" data-spy="scroll" data-target=".navbar" data-offset="150">

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top d-none">  
        <ul class="navbar-nav">
            <?php foreach ($data->quizzes as $quiz) { ?>
                <li class="nav-item">
                    <!-- <a class="nav-link" href="#quiz-<?php echo $quiz->id?>"></a> -->
                </li>    
            <?php } ?>
        </ul>
    </nav>

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top pt-1 pb-1" id="nav">
            <div class="col-md-* p-1 pr-1">
                <div class="btn text-light hover-text-primary" onclick="window.location.href='/user/home'">
                    <i class="fa fa-chevron-left"></i>
                    <span>Acasă</span>
                </div>
            </div>
            <div class="col-md-* pr-2 ml-auto">
                <div class="btn btn-lg hover-text-primary text-light" onclick="window.location.href='/user/quizEdit'">
                    <i class="fa fa-plus"></i>    
                </div>
            </div>
        </div>

        <div class="container p-0">
            <div class="row pt-4">
                <div class="col-md-2"></div>

                <div class="col-md-8 mb-4">
                    <div class="position-relative rounded smooth">
                        <h1 class="font-weight-light text-center">
                            Testele mele
                        </h1>
                    </div>
                    <div class="mt-5">
                        <?php foreach ($data->quizzes as $quiz) { ?>
                            <div class="position-relative rounded mt-3 p-3 shadow-box bg-lighter" id="quiz-<?php echo $quiz->id?>">

                                <div class="position-absolute text-<?php echo $quiz->active?"success":"danger"?>" style="top:3px;right:0px;font-size:10px;">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="quiz-switch-<?php echo $quiz->id?>" <?php echo $quiz->active?"checked":""?> onchange="changeQuizStatus(this, <?php echo $quiz->id?>)">
                                        <label class="custom-control-label" for="quiz-switch-<?php echo $quiz->id?>"></label>
                                    </div>
                                </div>
                            
                                <div class="d-flex flex-column">
                                    <h5 class="flex-fill font-weight-light mb-3">
                                        <span><?php echo$quiz->name?></span>
                                    </h5>
                                    <div class="flex-fill text-center">
                                        <div class="d-flex flex-row">
                                            <div class="flex-fill text-info">
                                                <i class="fas fa-question-circle"></i>
                                                <span><?php echo $quiz->questions_count?></span>
                                            </div>
                                            <div class="flex-fill text-success">
                                                <i class="fas fa-user"></i>
                                                <span><?php echo $quiz->unique_passed?></span>
                                            </div>
                                            <div class="flex-fill text-warning">
                                                <i class="fa fa-clock"></i>
                                                <span><?php echo isset($quiz->unique_medium_duration)?explode(".", $quiz->unique_medium_duration)[0]:"-"?>s</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-fill text-center mt-1">
                                        <div class="row">
                                            <div class="col-md-12 mt-2">
                                                <a href="/user/quizEdit/<?php echo$quiz->id?>" class="btn btn-block btn-outline-secondary">
                                                    <i class="fa fa-pen"></i>
                                                    <span>Editează</span>
                                                </a>
                                            </div>
                                            <!-- <div class="col-md-6 mt-2">
                                                <a href="#" class="btn btn-block btn-outline-danger">
                                                    <i class="fa fa-times"></i>
                                                    <span>Șterge</span>
                                                </a>
                                            </div> -->
                                        </div>
                                        
                                    </div>
                                </div>

                            </div>
                        <? } ?>
                    </div>
                </div>

                <div class="col-md-2"></div>
            </div>
        </div>    
    </div>

    <!-- Modal Question Edit -->
    <?php Loader::load("ModalQuestionEdit");?>

    <script>
        var quizId = parseInt(<?php echo isset($data->quiz)?$data->quiz->id:0?>);
        
        $("#quiz-title").focusout(function(){
            $.post("/user/saveQuiz<?php echo isset($data->quiz)?"/".$data->quiz->id:''?>", {title: $("#quiz-title").html(), cover: ""}, function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (resp.body !== false && resp.body !== true) {
                            window.location.replace("/user/quizEdit/"+resp.body.id);
                        }
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        })

        function enableDisableEdit(selector) {
            $(selector).focusout(function(e){
                $(e.target).attr("contenteditable", "false");
            });

            $(selector).attr("contenteditable", "true");
            $(selector).focus();
        }
        
        $(window).on('activate.bs.scrollspy', function (e) {
            var selector = $(".nav-link.active").attr("href");

            $("[spy]").addClass("text-secondary").removeClass("text-light");
            $(selector).removeClass("text-secondary").addClass("text-light");
            $(selector)
            .next().removeClass("text-secondary").addClass("text-light")
            .next().removeClass("text-secondary").addClass("text-light")
            .next().removeClass("text-secondary").addClass("text-light")
            .next().removeClass("text-secondary").addClass("text-light");
        });
        
        function changeQuizStatus(target, id){
            var url = "/quiz/enable/";
            if ($(target).attr("checked")) {
                url = "/quiz/disable/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

    </script>

</body>
</html>