<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rezultatele mele la teste</title>
    <?php Loader::load("Requires")?>
    <script>
        function makeHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[hidden-info]").css("opacity", "1");
            }, function(){
                $(selector).children("[hidden-info]").css("opacity", "0");
            });
        }
    </script>
</head>
<body class="bg-dark text-light">

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top pt-1 pb-1">
            <div class="col-md-* ml-auto p-1 pr-1">
                <button class="btn btn-darker" onclick="window.location.href='/user/home'">
                    <i class="fa fa-user"></i>
                    <span>Profil</span>
                </button>
            </div>
        </div>

        <div class="container p-0">
            <div class="row pt-4">
                <div class="col-md-3"></div>

                <div class="col-md-6 mb-4">
                    <div class="text-center">
                        <h2 class="font-weight-light">
                            Rezultatele la teste
                        </h2>
                        <h6 class="font-weight-light text-secondary btn-text-light">
                            <i class="fa fa-info-circle"></i>
                            <span>Rezultatele sunt calcultate doar pentru prima rezolvare a testului. Rezolvările ulterioare nu vor influența poziția ta în rating.</span>
                        </h6>
                    </div>

                    <?php foreach ($data->results as $result) { ?>
                        <div class="d-flex flex-column rounded position-relative shadow-light bg-light-1 mt-3 px-3 pt-4 pb-3" id="result-<?php echo$result->id?>">
                            <h5 class="flex-fill font-weight-light mb-3">
                                <span><?php echo$result->name?></span>
                            </h5>
                            <div class="flex-fill text-center">
                                <div class="d-flex flex-row">
                                    <div class="flex-fill text-warning">
                                        <i class="fa fa-trophy"></i>
                                        <span><?php echo substr($result->correct_percentage,0,5)?>%</span>
                                    </div>
                                    <div class="text-success flex-fill">
                                        <i class="fas fa-check-circle"></i>
                                        <span><?php echo $result->correct_answers?></span>
                                        <span class="text-secondary">/</span>
                                        <span class="text-secondary"><?php echo $result->questions_count?></span>
                                    </div>
                                    <div class="text-info flex-fill">
                                        <i class="fa fa-clock"></i>
                                        <span><?php echo (strtotime($result->datetime_stop)-strtotime($result->datetime_start))?>s</span>
                                    </div>
                                </div>
                            </div>
                            <div hidden-info class="position-absolute smooth text-center text-secondary w-100" style="bottom:-18px;opacity:0;font-size:13px;transition:0.5s">
                                <span><i class="far fa-calendar-alt mr-1"></i><?php echo date("j F, Y H:i", strtotime($result->datetime_start))?></span>
                                <span class="ml-4"><i class="fa fa-user mr-1"></i><?php echo $result->unique_players?></span>
                                <span class="ml-4"><i class="fa fa-eye mr-1"></i><?php echo $result->views?></span>
                            </div>
                        </div>
                        <script>makeHover("<?php echo "#result-".$result->id?>")</script>
                    <?php } ?>
                </div>

                <div class="col-md-3"></div>
            </div>
        </div>    
    </div>

    <?php Loader::load("Footer", array(
        "feedback" => true,
        "user" => true
    ));?>


</body>
</html>