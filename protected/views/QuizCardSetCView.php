<?php
    if (isset($data) && isset($data->quizzes) && (is_array($data->quizzes) || is_object($data->quizzes)) && isset($data->container)) { 
        $count = count($data->quizzes);
        $title = isset($data->title) ? $data->title : 'Folosește "title" => "..." pentru a seta un titlu';
        $extended = isset($data->extended) ? $data->extended : false;
        $showed = isset($data->showed) ? $data->showed : 2;
        $idPrefix = isset($data->idPrefix) ? $data->idPrefix : rand(10000, 99999)."-";
        $i = 0;

        // App::cLog($title . " : " . $count . " : " . $showed);

        if ($count) { ?>
            <div class="container p-0">
            <h1 class="font-weight-light text-center font-title pb-1"><?php echo $title?></h1>
            <div class="row p-0" id="<?php echo substr($data->container, 1)?>">
                <?php 
                    foreach ($data->quizzes as $quiz) {
                        $i++;
                        
                        Loader::load($i % 2?"QuizCard":"QuizCard", array(
                            "quiz" => $quiz,
                            "hidden" => $i > $showed ? true : false,
                            "extended" => $extended,
                            "idPrefix" => $idPrefix,
                            "backAnchor" => $i + 1 == $showed ? $data->container : false
                        ));
                    }
                ?>
            </div>
            </div>
            <?php 
                if ($count > $showed) {
                    Loader::load("ShrinkExtendC", array(
                        "container" => $data->container,
                        "extended" => $extended
                    ));
                }
            ?>
        <?php } ?>
<?php } ?>