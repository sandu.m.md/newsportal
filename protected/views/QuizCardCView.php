<?php if (isset($data->quiz)) { $quiz = &$data->quiz;?>
    <div class="position-relative rounded shadow-light bg-light-1 mt-3 p-3 <?php if(!$data->extended && $data->hidden) echo"d-none"?>" <?php if($data->hidden) echo "d-none"?> id="<?php echo $data->idPrefix.$quiz->id?>">
        <div class="d-flex flex-column">
            <h5 class="flex-fill font-weight-light mb-3">
                <span><?php echo$quiz->name?></span>
            </h5>
            <div class="flex-fill text-center">
                <div class="d-flex flex-row">
                    <div class="flex-fill text-info">
                        <i class="fas fa-question-circle"></i>
                        <span><?php echo $quiz->questions_count?></span>
                    </div>
                    <div class="flex-fill text-success">
                        <i class="fas fa-user"></i>
                        <span><?php echo $quiz->passed?></span>
                    </div>
                    <div class="flex-fill text-warning">
                        <i class="fa fa-clock"></i>
                        <span><?php echo isset($quiz->unique_medium_duration)?explode(".", $quiz->unique_medium_duration)[0]:"-"?>s</span>
                    </div>
                </div>
            </div>
            <div class="flex-fill text-center pt-3">
                <a href="/quiz/view/<?php echo$quiz->id?>" class="btn btn-block btn-outline-secondary">
                    <i class="fa fa-play"></i>
                </a>
            </div>
        </div>
        <div hidden-info class="position-absolute smooth text-center text-secondary w-100" style="bottom:-18px;left:0px;opacity:0;font-size:13px;transition:0.5s">
            <span><i class="far fa-calendar-alt mr-1"></i><?php echo date("j F, Y H:i", strtotime($quiz->datetime_created))?></span>
            <span class="ml-4"><i class="fa fa-user mr-1"></i><?php echo $quiz->owner_nick?></span>
        </div>
    </div>
    <script>
        addQuizHover('<?php echo "#".$data->idPrefix.$quiz->id?>');
    </script>
<?php } ?>