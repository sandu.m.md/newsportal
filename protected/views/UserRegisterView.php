<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Înregistrare</title>
    <?php Loader::load("Requires")?>

    <style>
    html, head, body {
        min-height: 100vh;
        margin: 0;
        padding: 0;
    }
    
    </style>
</head>
<body class="bg-dark text-light">
    
    <div class="container-fluid">
        <div class="row bg-dark sticky-top p-2 px-3">
            <div class="col-md-* d-flex align-items-center justify-content-center">
                <a href="/" class=""><div class="cover-image mr-1" style="background-image: url(/res/ico/logo.png);height:2rem;width:2rem;"></div></a>
                <a href="/" class="btn p-0 text-light font-title">4you</a>
            </div>
        </div>

        <div class="row pt-3">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form id="login-form" action="" onsubmit="return register()" class="pt-4">
                    <h1 class="font-weight-light mb-4 text-center">Înregistrare</h1>
                    <div class="form-group">
                        <label for="login-email">Adresa de email</label>
                        <input id="login-email" type="email" class="form-control form-control-lg" name="username" placeholder="ex. jorik@cardan.md" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="login-nick" id="login-nick-label">Numele dvs. sau un pseudonim</label>
                        <input id="login-nick" type="text" class="form-control form-control-lg" name="name" placeholder="ex. Jora Cardan" required>
                    </div>

                    <div class="form-group pt-3">
                        <label for="login-password" id="login-password-label">Întroduceți o parolă</label>
                        <input id="login-password" type="password" class="form-control form-control-lg" name="password" placeholder="Inventați o parolă" required>
                        
                    </div>

                    <div class="form-group">
                        <input id="login-password-repeat" type="password" class="form-control form-control-lg" name="password_repeat" placeholder="Repetați parola" required>
                    </div>
                    <button id="login-submit" type="submit" class="btn btn-lg btn-block btn-primary mt-4">Creare cont</button>
                    <div class="text-center pt-4">
                        <span>Ai deja un cont?</span>
                        <a href="/user/login" class="btn btn-sm btn-secondary">Autentifică-te</a>
                    </div>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    <?php Loader::load("Footer", array(
        "feedback" => true
    ));?>

    <script>

        $("#login-username").on("input", function(e){
            $("#login-submit").html("Conectare").removeClass("btn-danger").addClass("btn-primary");
        });

        $("#login-password").on("input", function(e){
            $("#login-password-label").html("Introduceți o parolă").removeClass("text-danger");
        });

        $("#login-nick").on("input", function(e){
            $("#login-nick-label").html("Numele dvs. sau un pseudonim").removeClass("text-danger");
        });

        function register() {
            if (!/^[A-Za-z0-9_\'-\.\,\!\?\(\)\[\]\s]{3,50}$/.test($("#login-nick").val())) {
                $("#login-nick-label").html("Pseudonimul dvs trebuie conțină de la 3 la 50 caractere și poate admite doar litere, cifre și caracterele ,._-'!?()[]").addClass("text-danger");
                return false;
            }

            if (!/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test($("#login-password").val())) {
                $("#login-password-label").html("Parola trebuie să conțină litere și cifre ale alfabetului latin și să nu conțină mai puțin de 8 caractere").addClass("text-danger");
                return false;
            }

            if ($("#login-password").val() !== $("#login-password-repeat").val()) {
                $("#login-password-label").html("Parolele nu coincid").addClass("text-danger");
                return false;
            }

            $("#login-submit").html('<div class="spinner-border"></div>');

            setTimeout(function(){
            $.post("/user/register", $("#login-form").serializeArray(), function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        $("#login-submit").removeClass("btn-primary").addClass("btn-success");

                        setTimeout(function(){
                            console.log("Redirect to home");
                            window.location.href = "/user/home";
                        }, 1000);
                    } else {
                        console.log(resp.info.message);
                        $("#login-submit").html("Eroare").removeClass("btn-primary").addClass("btn-danger");
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            })

            }, 1000);

            return false;
        };
    
    </script>
</body>
</html>