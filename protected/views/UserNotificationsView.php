<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TopTest - Testele mele</title>

    <?php Loader::load("Requires")?>
    <style>
        html, head, body {
            margin: 0;
            padding: 0;
            min-height: 100vh;
        }
    
        body {
            overflow-x: hidden;
        }

        .bg-light-1 {
            background-color: rgba(255, 255, 255, 0.075);
        }

        .shadow-light {
            transition: 0.4s;
            cursor: pointer;
            box-shadow: 3px 3px 15px rgba(0,0,0,0.2);
        }

        .shadow-light:hover {
            transform: translateY(-8px);
            box-shadow: 11px 11px 23px rgba(0,0,0,0.4);
            color: #f8f9fa!important;
        }

        .smooth {
            transition: 0.4s;
        }
    </style>
    <script>
        function addNotificationHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[n-hidden-info]").css("opacity", "1");
                $(selector).children("[n-subject]").addClass("text-light");
                $(selector).children("[n-text]").addClass("text-light");
                $(selector).children("[n-from]").addClass("text-info");
            }, function(){
                $(selector).children("[n-hidden-info]").css("opacity", "0");
                $(selector).children("[n-subject]").removeClass("text-light");
                $(selector).children("[n-text]").removeClass("text-light");
                $(selector).children("[n-from]").removeClass("text-info");
            });
        }
    </script>
</head>
<body class="bg-dark text-light" data-spy="scroll" data-target=".navbar" data-offset="150">

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top d-none">  
        <ul class="navbar-nav">
            <?php foreach ($data->quizzes as $quiz) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="#quiz-<?php echo $quiz->id?>"></a>
                </li>    
            <?php } ?>
        </ul>
    </nav>

    <div class="container-fluid">

        <!-- Topbar -->
        <div class="row shadow-lg bg-dark sticky-top pt-1 pb-1" id="nav">
            <div class="col-md-* p-1 pr-1">
                <button class="btn text-light hover-text-primary" onclick="window.location.href='/user/home'">
                    <i class="fa fa-chevron-left"></i>
                    <span>Acasă</span>
                </button>
            </div>
            <div class="col-md-* p-1 pr-2 ml-auto">
                <!-- <button class="btn btn-text-light text-secondary" onclick="window.location.href='/user/quizEdit'">
                    <i class="fa fa-plus"></i>    
                </button> -->
            </div>
        </div>

        <div class="container p-0">
            <div class="row pt-4">
                <div class="col-md-3"></div>

                <div class="col-md-6 mb-4">

                    <h1 class="font-weight-light text-light text-center">
                        <span>Notificări</span>
                    </h1>

                    <h6 class="font-weight-light text-secondary text-center">
                        <i class="fa fa-info-circle"></i>
                        <span>Pentru a marca o notificare ca fiind citită faceți click pe aceasta sau atingeți cu degetul.</span>
                    </h6>
                    <div class="mt-5">
                        <?php foreach ($data->notifications as $notification) { ?>
                            <div class="rounded position-relative shadow-light bg-light-1 mt-3 px-3 pt-2 pb-4 text-light" id="notification-<?php echo$notification->id?>" <?php if(!$notification->seen)echo'onclick="seen('.$notification->id.')"'?>>
                                <h3 n-subject class="smooth <?php echo$notification->seen?"text-secondary":""?>">
                                    <span><?php echo$notification->subject?></span>
                                </h3>
                                <h6 n-text class="font-weight-light smooth <?php echo$notification->seen?"text-secondary":""?>">
                                    <?php echo$notification->text?>
                                </h6>
                                <div n-from class="position-absolute text-secondary smooth" style="bottom:5px;right:10px;font-size:13px;">
                                    <span>De la</span>
                                    <span><?php echo$notification->from_nick?></span>
                                </div>
                                <div n-hidden-info class="position-absolute text-center text-secondary w-100" style="bottom:-18px;left:0px;opacity:0;font-size:13px;transition:0.5s">
                                    <span><i class="far fa-calendar-alt mr-1"></i><?php echo date("j F, Y H:i", strtotime($notification->datetime_sent))?></span>
                                </div>
                            </div>
                            <script>addNotificationHover("<?php echo "#notification-".$notification->id?>")</script>
                        <? } ?>
                    </div>
                </div>

                <div class="col-md-3"></div>
            </div>
        </div>    
    </div>

    <?php Loader::load("Footer", array(
        "feedback" => true,
        "user" => true
    ));?>

    <!-- Modal Question Edit -->
    <?php Loader::load("ModalQuestionEdit");?>

    <script>
        function seen(notifId) {
            $.post("/user/seenNotification/"+notifId, {}, function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        var notif = $("#notification-"+notifId);
                        notif.children("[n-subject]").addClass("text-secondary");
                        notif.children("[n-text]").addClass("text-secondary");
                        notif.unbind("click");
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

    </script>

</body>
</html>