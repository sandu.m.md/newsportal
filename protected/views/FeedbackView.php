<!DOCTYPE html>
<html lang="en">
<head>
  <title>Thank You</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="refresh" content="5	;url=/quiz"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
      	body{
        	height:100vh;
      	}
  </style>
</head>
<body>

	<div class="d-flex flex-column h-100">
		<div class="px-0 pt-4 pb-4  bg-dark text-light">
			<h1 class="text-center">Vă mulțumim pentru feedback!</h1>
			<p class="text-center">Curând veți fi redirecționat, joc plăcut în continuare!</p> 
		</div>
		<div class="d-flex justify-content-center align-items-center">
			<img src="/res/images/ok.png" class="img-full" alt="Multumesc">
		</div>
	</div>

	
</body>
</html>
