<?php if ($data) {?>
<div class="mt-3 pt-1 text-center" style="cursor:pointer;" id="<?php echo substr($data->container, 1)?>-anchorr">
    <a class="btn btn-outline-secondary <?php if(!$data->extended) echo 'd-none'?>" style="text-decoration:none;" onclick="shrinkQuizzes(this, '<?php echo$data->container?>');" href="<?php echo $data->container?>-anchorr">
        <i class="fa fa-angle-up"></i>
        <span>Mai puține</span>
    </a>
    <span class="btn btn-outline-secondary <?php if($data->extended) echo 'd-none'?>" onclick="extendQuizzes(this, '<?php echo$data->container?>')">
        <i class="fa fa-angle-down"></i>
        <span>Mai multe</span>
    </span>
</div>
<?php } ?>