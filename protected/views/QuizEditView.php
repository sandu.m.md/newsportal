<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php Loader::load("Requires"); ?>
</head>
<body class="bg-light">
    
    <div class="container-fluid">
        <div class="row p-2 bg-dark text-light sticky-top">
            <div class="col-md-*">
                <a href="/admin" class="btn btn-sm btn-success">Acasă</a>
            </div>
            <div class="col-md-* ml-auto">
                <!-- <button class="btn btn-sm rounded-circle btn-danger" title="Resetează statistica"><i class="fa fa-undo"></i></button> -->
            </div>
            <div class="col-md-* ml-2">
                <input type="checkbox" <?php echo isset($data->quiz)&&$data->quiz->active?"checked":"";?> data-toggle="toggle" data-on="Activ" data-off="Inactiv" data-size="sm" data-style="ios" <?php echo isset($data->quiz)?:'disabled'?> onchange="changeQuizStatus(this, <?php echo isset($data->quiz)?$data->quiz->id:0?>)">
            </div>
            <div class="col-md-* ml-2">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#question-edit-modal" onclick="clearModal()">+întrebare</button>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="row mb-5">
                    <div class="col-md-4">
                        <h3><span class="badge badge-info">Imaginea de copertă</span></h3>
                        <div class="card text-white">
                            <img src="<?php echo isset($data->quiz)&&isset($data->quiz->cover) ? $data->quiz->cover:"/res/images/2019_12_15_00_55_45_784027.jpg"?>" class="card-img rounded-bottom" alt="Adaugă o imagine" id="quiz-cover">
                            <div class="card-img-overlay">
                                <div class="btn btn-primary position-absolute rounded pl-3 pr-3" style="top:0;right:0;" title="Schimbă imaginea" data-toggle="modal" data-target="#myModal" onclick="$('[name=img-type]').val('cover');">
                                    <i class="fa fa-plus"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3><span class="badge badge-primary">Titlu</span></h3>
                        <h4 class="font-weight-light" id="post-title">
                            <span title="Click pentru a edita titlul" contenteditable="true" id="quiz-title"><?php echo isset($data)?$data->quiz->name:"Click pentru a adăuga un titlu"?></span>
                        </h4>
                    </div>
                    <div class="col-md-4">
                        <h3 class="font-weight-light">
                            <span class="badge badge-success">Scara de evaluare</span>
                        </h3>

                        <?php if (isset($data->ratingScale)) { ?>
                            <h5><span class="badge badge-secondary">0-20%</span></h5>
                            <h5 class="font-weight-light"><?php echo $data->ratingScale->text_1?></h5>

                            <h5><span class="badge badge-secondary">20-40%</span></h5>
                            <h5 class="font-weight-light"><?php echo $data->ratingScale->text_2?></h5>

                            <h5><span class="badge badge-secondary">40-60%</span></h5>
                            <h5 class="font-weight-light"><?php echo $data->ratingScale->text_3?></h5>

                            <h5><span class="badge badge-secondary">60-80%</span></h5>
                            <h5 class="font-weight-light"><?php echo $data->ratingScale->text_4?></h5>

                            <h5><span class="badge badge-secondary">80-100%</span></h5>
                            <h5 class="font-weight-light"><?php echo $data->ratingScale->text_5?></h5>
                        <?php } ?>
                    </div>
                    
                </div>
                <table class="table table-striped table-hover mb-5">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Textul întrebării</th>
                            <th>Răspuns 1</th>
                            <th>Răspuns 2</th>
                            <th>Răspuns 3</th>
                            <th>Fără răspuns</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0;if(isset($data)&&isset($data->q))foreach ($data->q as $qid => $q) { $i++?>
                            <tr id="q-<?php echo $q->id?>">
                                <input type="hidden" id="q-<?php echo $q->id?>-correct" value="<?php echo $q->correct_answer?>">
                                <td><?php echo $i?></td>
                                <td id="q-<?php echo $q->id?>-question"><?php echo $q->question?></td>
                                <td><h6><span  id="q-<?php echo $q->id?>-answer-1" <?php if($q->correct_answer==1)echo'class="p-1 rounded bg-success text-light"'?>><?php echo $q->answer_1?></span> <span class="badge badge-<?php echo $q->choosed_1>0?'primary':'secondary'?>"><?php echo $q->choosed_1?></span></h6></td>
                                <td><h6><span id="q-<?php echo $q->id?>-answer-2" <?php if($q->correct_answer==2)echo'class="p-1 rounded bg-success text-light"'?>><?php echo $q->answer_2?></span> <span class="badge badge-<?php echo $q->choosed_2>0?'primary':'secondary'?>"><?php echo $q->choosed_2?></span></h6></td>
                                <td><h6><span  id="q-<?php echo $q->id?>-answer-3" <?php if($q->correct_answer==3)echo'class="p-1 rounded bg-success text-light"'?>><?php echo $q->answer_3?></span> <span class="badge badge-<?php echo $q->choosed_3>0?'primary':'secondary'?>"><?php echo $q->choosed_3?></span></h6></td>
                                <td><h6><span class="badge badge-<?php echo $q->choosed_0>0?'warning':'secondary'?>"><?php echo $q->choosed_0?></span></h6></td>
                                <td><input type="checkbox" data-toggle="toggle" <?php if($q->active)echo 'checked';?> data-size="sm" data-style="ios" onchange="changeQuestionStatus(this,<?php echo $q->id?>);"></td>
                                <td><button class="btn btn-sm text-primary" data-toggle="modal" data-target="#question-edit-modal" onclick="fillModal(<?php echo $q->id?>);"><i class="fa fa-pen"></i></button></td>
                                <td><button class="btn btn-sm text-danger" onclick="removeQuestion('Sigur doriți să ștergeți întrebarea „<?php echo$q->question?>”?', parseInt(<?php echo $q->id?>));"><i class="fa fa-times"></i></button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <form id="modal-upload-img-form" onsubmit="return uploadFile();">
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Încărcați o imagine</h4>
                        <button type="button" class="close" data-dismiss="modal" id="upload-img-close">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="modal-upload-img">
                            <label class="custom-file-label" name="modal-upload-img">Alegeți</label>
                        </div>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Ok">
                    </div>

                    <!-- Hidden -->
                    <input type="hidden" name="img-type">
                </div>
            </div>
        </div>
    </form>


    <!-- The Modal -->
    <form id="edit-question-form" onsubmit="return saveQuestion();">
        <div class="modal mt-5" id="question-edit-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3 class="font-weight-light text-dark">Editează câmpurile</h3>
                        <button type="button" class="close" data-dismiss="modal" id="upload-img-close-">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="custom-file">
                            <input type="hidden" value="0" name="modal-qid">
                            <input type="hidden" value="<?php echo $data->quiz->id?>" name="modal-quiz-id">

                            <h5 class="pb-1"><span class="bg-primary rounded text-light p-1 pl-2 pr-2">Textul întrebării</span></h5>
                            <input type="text" class="form-control" name="modal-question" placeholder="Ce? Unde? Când?" required autocomplete="off">

                            <h5 class="pb-1 pt-4"><span class="bg-danger rounded text-light p-1 pl-2 pr-2">Variantele de răspuns</span></h5>
                            <input type="text" class="form-control mb-2" name="modal-answer-1" placeholder="Varianta 1" required autocomplete="off">
                            <input type="text" class="form-control mb-2" name="modal-answer-2" placeholder="Varianta 2" required autocomplete="off">
                            <input type="text" class="form-control mb-2" name="modal-answer-3" placeholder="Varianta 3" required autocomplete="off">

                            <h5 class="pb-1 pt-4"><span class="bg-success rounded text-light p-1 pl-2 pr-2">Varianta corectă</span></h5>

                            <select name="modal-correct" class="custom-select">
                                <option value="1">#1</option>
                                <option value="2">#2</option>
                                <option value="3">#3</option>
                            </select>                            
                        </div>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-block btn-lg btn-success" value="Salvează">
                    </div>

                    <!-- Hidden -->
                    <input type="hidden" name="img-type">
                </div>
            </div>
        </div>
    </form>

    <script>
        var cover = $("#quiz-cover");

        $(document).on('data-attribute-changed', function() {
            var data = cover.data('mydata');
            alert('Data changed to: ' + data);
        });


        $("#quiz-title").focusout(function(){
            $.post("/quiz/saveQuiz<?php echo isset($data)?"/".$data->quiz->id:''?>", {title: $("#quiz-title").html(), cover: cover.attr("src")}, function(data){
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (resp.body !== false && resp.body !== true) {
                            window.location.replace("/quiz/edit/"+resp.body.id);
                        }
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });
        })

        function changeQuizStatus(target, id){
            var url = "/quiz/enable/";
            if ($(target).attr("checked")) {
                url = "/quiz/disable/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

        function changeQuestionStatus(target, id) {
            var url = "/quiz/enableQuestion/";
            if ($(target).attr("checked")) {
                url = "/quiz/disableQuestion/";
            }

            $.post(url+id, {}, function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        if (/enable.*/.test(url)) {
                            $(target).attr("checked", true);    
                        } else {
                            $(target).removeAttr("checked");
                        }
                    } else {
                        if (/enable.*/.test(url)) {
                            $(target).removeAttr("checked");   
                        } else {
                            $(target).attr("checked", true);
                        }
                    }
                } catch(ex) {
                    console.log(ex.message);
                }
            });
        }

        function removeQuestion(msg, qid) {
            if(confirm(msg)) {
                $.post('/quiz/removeQuestion/'+qid, {}, function(data){
                    try {
                        var resp = JSON.parse(data);

                        if (resp.ok) {
                            location.reload();
                        } else {
                            alert("Ceva nu a funcționat corect");
                        }
                    } catch (ex) {
                        console.log(ex.message);
                    }
                });
            }
        }

        function clearModal() {
            $("[name=modal-qid]").val(0);
            $("[name=modal-question]").val('');
            $("[name=modal-answer-1]").val('');
            $("[name=modal-answer-2]").val('');
            $("[name=modal-answer-3]").val('');
            $("[name=modal-correct]").val(1);
            setTimeout(function(){$('[name=modal-question]').focus();}, 100);
        }

        function fillModal(qid) {
            $("[name=modal-qid]").val(qid);
            $("[name=modal-question]").val($("#q-"+qid+"-question").html());
            $("[name=modal-answer-1]").val($("#q-"+qid+"-answer-1").html());
            $("[name=modal-answer-2]").val($("#q-"+qid+"-answer-2").html());
            $("[name=modal-answer-3]").val($("#q-"+qid+"-answer-3").html());
            $("[name=modal-correct]").val($("#q-"+qid+"-correct").val());
            setTimeout(function(){$('[name=modal-question]').focus();}, 100)
        }

        function saveQuestion() {
            $.post("/quiz/editQuestion", $("#edit-question-form").serializeArray(), function(data) {
                try {
                    var resp = JSON.parse(data);

                    if (resp.ok) {
                        location.reload();
                    } else {
                        console.log("Bad");
                    }
                } catch (ex) {
                    console.log(ex.message);
                }
            });

            return false;
        }

        function uploadFile() {
            var url = "/api/uploadImg";
            var file_data = $('#modal-upload-img').prop('files')[0];   
            var form_data = new FormData($("#modal-upload-img-form")[0]);                  
            form_data.append('file', file_data);

            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data){
                    try {
                        var resp = JSON.parse(data);

                        if (resp.ok) {
                            cover.attr("src", resp.body.filename);

                            $("#quiz-title").trigger("focusout");
                        } else {
                            console.log("Bad");
                        }
                    } catch (ex) {
                        console.log(ex.message);
                    }
                }
            });
            $("#upload-img-close").trigger("click");

            return false;
        }

    </script>

</body>
</html>