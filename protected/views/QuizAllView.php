<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cele mai interesante teste! Rapid și simplu!</title>
    <?php Loader::load("Requires")?>

    <style>
        /* html, head, body {
        } */
    </style>

    <script>
        function addQuizHover(selector) {
            $(selector).hover(function(e){
                $(selector).children("[hidden-info]").css("opacity", "1");
                $(selector).children("[q-from]").addClass("text-info");
            }, function(){
                $(selector).children("[hidden-info]").css("opacity", "0");
                $(selector).children("[q-from]").removeClass("text-info");
            });
        }

        $(document).ready(function() {
            $("#topbar").trigger("click");
        });
    </script>
</head>
<body class="bg-dark text-light">
    
    <a href="#topbar" id="topbar"></a>

    <div class="container-fluid">

        <div class="row d-flex" style="min-height: 100vh">
            <div class="col-md-12 d-flex flex-column bg-1 rounded-bottom-xl shadow" style="height:50vh;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="d-flex p-3 w-100">
                                <div class="text-left" style="width:33.33vw;">
                                    <a href="/user/top" id="a-newest" class="btn btn-darker"><i class="far fa-star mr-1"></i>Top</a>
                                </div>
                                <div class="text-center" style="width:33.33vw;">
                                    <a href="/quiz" class="btn btn-lg text-light hover-text-light"><h2 class=" font-weight-light m-0 p-0"><i class="fa fa-home"></i></h2></a>
                                </div>
                                <div class="text-right" style="width:33.33vw;">
                                <?php if (App::$user->isLoggedIn()) { ?>
                                        <a href="/user/home" class="btn btn-darker"><i class="fa fa-user-alt mr-1"></i>Profil</a>
                                    <?php } else { ?>
                                        <a href="/user/login" class="btn btn-darker"><i class="fa fa-user-alt mr-1"></i>Login</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex-fill d-flex justify-content-center align-items-center">
                    <h1 class="font-weight-light">You're home!</h1>
                </div>
            </div>

            <div class="col-md-12 d-flex justify-content-center align-items-center" style="height:50vh">
                <a href="#main-content">
                    <div class="btn text-secondary hover-text-danger" id="scroll-down">
                        <i class="fa fa-angle-double-down display-1"></i>
                    </div>
                </a>
            </div>
        </div>
        
        
        <a href="#main-content" id="main-content"></a>
        <div class="row pt-5">

            <div class="col-md-12">
                <div>
                    <?php Loader::load("QuizCardSetC", array(
                        "quizzes" => QuizModel::getRecommended(),
                        "title" => "Recomandări",
                        "container" => "#recommended",
                        "showed" => 6
                    ))?>
                </div>

                <div class="pt-5">
                    <?php Loader::load("QuizCardSetC", array(
                        "quizzes" => QuizModel::getNewest(),
                        "title" => "Cele mai noi",
                        "container" => "#newest",
                        "showed" => 6
                    ))?>
                </div>

                <div class="pt-5">
                    <?php Loader::load("QuizCardSetC", array(
                        "quizzes" => QuizModel::getTop(),
                        "title" => "Interesante",
                        "container" => "#top",
                        "showed" => 6
                    ))?>
                </div>
    
            </div>
            <!-- <div class="col-md-3">Ad</div> -->
        </div>
    </div>

    <?php Loader::load("Footer", array(
        "feedback" => true,
        "user" => true
    ));?>

    <script>

        var count = 2;

        setInterval(function(){
            if (count > 0) {
                $("#scroll-down").trigger("mouseenter");
            }
        }, 1200);

        setTimeout(function(){
            setInterval(function(){
                if (count > 0) {
                    $("#scroll-down").trigger("mouseleave");
                    count--;
                }
            }, 1200);
        }, 600);

        $("#scroll-down").hover(function(){
            $("#scroll-down").css({
                "transition": "0.6s",
                "transform": "translateY(30px)"
            });
        }, function(){
            $("#scroll-down").css({
                "transition": "0.6s",
                "transform": "translate(0px)"
            });
        });
        
    </script>
</body>
</html>