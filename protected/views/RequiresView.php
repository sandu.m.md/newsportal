<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<!-- google ads -->
<script data-ad-client="ca-pub-4378826952178726" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<!-- input switches -->
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20rem; }
  .toggle.ios .toggle-handle { border-radius: 20rem; }
</style>


<!-- Draggable  -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="/js/makeId.js"></script>

<!-- <link href="/css/main.css" rel="stylesheet"> -->

<!-- <link rel="shortcut icon" type="image/png" href="/res/ico/favicon_16x16_2.png"/> -->
<!-- <link rel="shortcut icon" type="image/png" href="/res/ico/favicon_96x96_1.png"/> -->
<!-- <link rel="shortcut icon" type="image/png" href="/res/ico/ico_32x32_1.png"/> -->
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Cairo:200,300&display=swap&subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400&display=swap&subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,300&display=swap&subset=latin-ext" rel="stylesheet">


<!-- Texts -->
<style>
	.hover-text-light:hover {
		color: #f8f9fa!important;
	}

	.hover-text-primary:hover {
		color: #007bff!important;
	}

	.hover-text-warning:hover {
		color: #ffc107!important;
	}

	.hover-text-danger:hover {
		color: #dc3545!important;
	}

	.hover-text-success:hover {
		color: #28a745!important;
	}

	.hover-text-dark:hover {
		color: #343a40!important;
	}

	.text-orange {
		color: #FF8C00;
	}

	.text-special {
		color: #f0027f;
	}

	.text-lighter {
		color: #f8f9faaa!important;
	}

	.font-title {
		font-family: 'Ubuntu', sans-serif;
		font-weight: 300; 
	}
</style>


<style>
	/* .tooltip {
		position: relative;
		display: inline-block;
	}

	.tooltip .tooltiptext {
		visibility: hidden;
		width: 120px;
		background-color: black;
		color: #fff;
		text-align: center;
		border-radius: 6px;
		padding: 5px 0;

		position: absolute;
		z-index: 1;
	}

	.tooltip:hover .tooltiptext {
		visibility: visible;
	} */
</style>

<!-- Backgrounds -->
<style>
	.zoom {
		transition: 0.3s;
		cursor: pointer;
	}

	.zoom:hover {
		transform: scale(1.22);
	}

	.hover-bg-lighter:hover {
		background-color: rgba(255, 255, 255, 0.075);
	}

	.bg-ico-1 {
		background-image: linear-gradient(to left, #e43a15, #e65245);
	}

	.bg-ico-2 {
		background-image: linear-gradient(to right, #7474BF, #348AC7)
	}

	.bg-fb {
		background-color: #3b5998;
	}

	.rounded-bottom-xl {
		border-bottom-left-radius: 15px!important;
		border-bottom-right-radius: 15px!important;
	}

	.rounded-xl {
		border-radius: 10px!important;
	}

	.bg-lighter {
		background-color: rgba(255, 255, 255, 0.075);
	}

	.bg-darker {
		background-color: rgba(0, 0, 0, 0.6);
	}

	.bg-2nd {
		background-color: #FF8C00;
	}

	.bg-soundcloud {
		background-image: linear-gradient(to left, #fe8c00, #f83600);
	}

	.bg-silver {
		background-color: #DDDDDD99;
	}

	.bg-light-1 {
		background-color: rgba(255, 255, 255, 0.075);
	}

	.bg-banner {
		background: #2BC0E4;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #EAECC6, #2BC0E4);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #EAECC6, #2BC0E4); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-1 {
		background: #2193b0;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-2 {
		background: #2193b0;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-3 {
		background: #654ea3;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #eaafc8, #654ea3);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #eaafc8, #654ea3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-4 {
		background: #FDC830;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #F37335, #FDC830);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #F37335, #FDC830); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-5 {
		background: #00B4DB;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #0083B0, #00B4DB); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-banner-6 {
		background: #EB3349;  /* fallback for old browsers */
		background: -webkit-linear-gradient(to right, #F45C43, #EB3349);  /* Chrome 10-25, Safari 5.1-6 */
		background: linear-gradient(to right, #F45C43, #EB3349); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
	}

	.bg-special {
		background-image: linear-gradient(to right, #f0027f, #77479e);
	}

	.hover-bg-lighter:hover {
		background-color: rgba(255, 255, 255, 0.5)!important;
	}

	.hover-bg-darker:hover {
		background-color: rgba(0, 0, 0, 0.6)!important;
	}

	.btn-darker {
		color: #f8f9fa!important;
	}

	.btn-darker:hover {
		background-color: rgba(0, 0, 0, 0.6)!important;
		color: #f8f9fa!important;
	}
</style>


<!-- Covers -->
<style>
	.cover {
		border-radius: inherit;
		width: 100%;
		height: 100%;
	}

	.cover-darker {
		background-color: rgba(0, 0, 0, 0.6);
	}

	.cover-darker-1 {
		background-color: rgba(0, 0, 0, 0.5);
	}

	.cover-fadeout {
		transition: 0.4s;
	}

	.cover-fadeout:hover {
		background-color: transparent!important;
		/* background: none; */
	}

	.cover-image {
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
	}

	.cover-fade-to-bottom {
		/* background: -webkit-linear-gradient(to bottom, #0083B0, #00B4DB); */
		/* rgba(0, 0, 0, 0.7) */
		/* rgba(0, 0, 0, 0.9) */
		
		/* background: linear-gradient(to bottom, #343a40D0, #343a4080, rgba(0, 0, 0, 0.2)); */
		background: linear-gradient(to bottom, #007bffff, #007bffff, #007bff80, #007bff00);
	}

	.cover-fade-to-left {
		/* background: -webkit-linear-gradient(to bottom, #0083B0, #00B4DB); */
		/* background: linear-gradient(to bottom left, rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.2)); */
	
		/* background: linear-gradient(to left, #f8f9faff, #f8f9fa88, #f8f9fa00); */
		/* background: linear-gradient(to left, #0074D9ff, #0074D9A0, #f8f9fa00); */
		/* background: linear-gradient(to top, #007bffff, #007bffA0, #007bff00); */
		/* background: linear-gradient(to left, #4267b2ff, #4267b2A0, #4267b200); */
		
		/* background: linear-gradient(to left, #343a40ff, #343a40A0, #343a4000); */
	}

	.cover-fade-to-right {
		/* background: linear-gradient(to right, #343a40ff, #343a40A0, #000000ff); */
		/* background: linear-gradient(to right, #343a40ff, #343a40A0, #343a4000); */
		background: linear-gradient(to right, #007bffff, #007bffA0, #007bff00);
	}

	

</style>


<!-- Others -->
<style>
  	html, head, body {
		padding: 0;
		margin: 0;
	}

	.btn-text-light {
		transition: 0.4s;
		text-decoration: none;
		color: #6c757d!important;
	}

	.btn-text-light:hover {
		color: #f8f9fa!important;
	}

	a.hover-text-dark:hover {
		color: #343a40!important;
	}

	.shadow-box {
		transition: 0.3s;
		cursor: pointer;
		box-shadow: 3px 3px 15px rgba(0, 0, 0, 0.2);
	}

	.shadow-box:hover {
		transform: translateY(-5px);
		box-shadow: 8px 8px 20px rgba(0, 0, 0, 0.4);
		color: #f8f9fa!important;
	}

	.shadow-light {
		transition: 0.4s;
		cursor: pointer;
		box-shadow: 3px 3px 15px rgba(0,0,0,0.2);
	}

	.shadow-light:hover {
		transform: translateY(-5px);
		box-shadow: 8px 8px 20px rgba(0,0,0,0.4);
		color: #f8f9fa!important;
	}

	.fadein {
		opacity:0;
		transition: 0.4s;
	}

	.fadein:hover {
		opacity: 1;
	}

	a {
		color: inherit;
		text-decoration: none!important;
	}

</style>


<!-- colors -->
<style>

	.clr {
		color: #368e81;
	}

	.bg-1 {
		background-image: linear-gradient(to top, #2B32B2, #1488CC);
	}

</style>

<script>
	function shrinkQuizzes(target, containerSelector) {
		$(containerSelector).children('[d-none]').addClass('d-none');
		$(target).addClass("d-none");
		$(target).next().removeClass("d-none");
		// console.log($(containerSelector).children('[d-none]'));
		// console.log("Epta");
	}

	function extendQuizzes(target, containerSelector) {
		$(containerSelector).children('[d-none]').removeClass('d-none');
		$(target).addClass("d-none");
		$(target).prev().removeClass("d-none");
	}

	$(document).on('click', 'a[href^="#"]', function (event) {
		event.preventDefault();

		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 1000);
	});

	function getRotationDegrees(obj) {
		var matrix = obj.css("-webkit-transform") ||
		obj.css("-moz-transform")    ||
		obj.css("-ms-transform")     ||
		obj.css("-o-transform")      ||
		obj.css("transform");
		if(matrix !== 'none') {
			var values = matrix.split('(')[1].split(')')[0].split(',');
			var a = values[0];
			var b = values[1];
			var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
		} else { var angle = 0; }
		return (angle < 0) ? angle + 360 : angle;
	}

	function animateGoDownUp(selector, times = 1, duration = 0.6) {
		$(selector).hover(function(){
			$(selector).css({
				"transition": duration+"s",
				"transform": "translateY(20px)"
			});
		}, function(){
			$(selector).css({
				"transition": duration+"s",
				"transform": "translate(0px)"
			});
		});

		for (var i = 1; i <= times; i++) {
			setTimeout(function(){
				$(selector).trigger("mouseenter");
			}, i * duration * 2000);

			setTimeout(function(){
				$(selector).trigger("mouseleave");
			}, i * duration * 2000 + duration * 1000);
		}
	}

	function animateZoomDecrease(selector, times = 1, duration = 0.6) {
		$(selector).hover(function(){
			$(selector).css({
				"transition": duration+"s",
				"transform": "translateY(-5px) scale(1.05)"
			});
		}, function(){
			$(selector).css({
				"transition": duration+"s",
				"transform": "translateY(0) scale(1)"
			});
		});

		for (var i = 1; i <= times; i++) {
			setTimeout(function(){
				$(selector).trigger("mouseenter");
			}, i * duration * 2000);

			setTimeout(function(){
				$(selector).trigger("mouseleave");
			}, i * duration * 2000 + duration * 1000);
		}
	}

	function animateFadeIn(selector, duration = 0.6, timeout = 0, rotate = 0) {
		setTimeout(function() {
			var angle = getRotationDegrees($(selector)) + rotate;
			$(selector).css({
				"transition": duration+"s",
				"transform": "scale(1) rotate("+angle+"deg)",
				"opacity": "1"
			});
		}, timeout * 1000);
	}

	function animateRotate(selector, direction = "right", speed = 1) {
		var angle = getRotationDegrees($(selector));

		// console.log("Unghi: " + angle);
		setInterval(function() {
			angle += direction == "right" ? speed : -speed;
			$(selector).css({
				"transform": "rotate("+angle+"deg)"
			});
		}, 50);
	}

</script>
