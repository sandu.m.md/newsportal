<?php
    $feedback = isset($data->feedback) ? $data->feedback : false;
    $user = isset($data->user) ? $data->user : false;
    $quizEdit = isset($data->quizEdit) ? $data->quizEdit : false;
?>
<div class="container-fluid shadow shadow-lg p-3 mt-5 <?php echo App::$theme->bg->color_footer . " " . App::$theme->text->color_footer?>">
    <div class="row">
        <div class="col-md-4"></div>

        <div class="col-md-4">
            <h4 class="text-center">
                <i class="far fa-smile text-secondary btn-text-light"></i><br>
            </h4>
            <h6 class="font-weight-light text-center text-secondary">
                Acest site este în curs de dezvoltare.<br>
                Ne cerem scuze de problemele care pot apărea.
            </h6>
        </div>

        <div class="col-md-4 text-right">
            <?php if ($feedback) { ?>
                <div class="text-center">
                    <div class="btn text-light hover-text-primary" onclick="window.location.href='/help'"><i class="far fa-comments mr-1 text-warning"></i>Feedback</div>
                </div>
            <?php } ?>
            <?php if ($user && false) { ?>
                <div class="text-center">
                    <?php if (App::$user->isLoggedIn()) { ?>    
                        <div class="btn btn-sm text-light hover-text-primary" onclick="window.location.href='/user/home'"><i class="far fa-user mr-1"></i>Profil</div>
                    <?php } else { ?>
                        <div class="btn btn-sm text-light hover-text-primary" onclick="window.location.href='/user/login'"><i class="far fa-user mr-1"></i>Login</div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($quizEdit && App::$user->isLoggedIn() && App::$user->isManager()) { ?>
                <div class="text-center">
                    <div class="btn btn-sm text-light hover-text-primary" onclick="window.location.href='/user/quizEdit/<?php echo$quizEdit?>'"><i class="fa fa-pen mr-1"></i>Editează</div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="font-weight-light text-secondary text-center">Copyright &copy; <?php echo date("Y");?></h6>
        </div>
    </div>
</div>