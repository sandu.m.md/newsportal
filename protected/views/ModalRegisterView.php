<!-- The Modal -->
<div class="modal mt-5" id="modal-register" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3 class="font-weight-light text-dark">Înregistrează-te pentru a-ți salva rezultatele la test!</h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <!-- <div class="modal-body">
                <div class="custom-file">
                    <input type="text" class="form-control" name="email" placeholder="Adresa de email" required>

                </div>
            </div> -->
            
            <!-- Modal footer -->
            <div class="modal-footer pt-4">
                <a href="/user/register" class="btn btn-lg btn-block btn-success">Înregistrare</a>
            </div>

            <!-- Hidden -->
            <!-- <input type="hidden" name="img-type"> -->
        </div>
    </div>
</div>
<button style="display:none" id="modal-register-show" data-toggle="modal" data-target="#modal-register"></button>
<script>
    // function modalRegister() {
    //     $("#modal-register").show();
    // }
    // function subscribe() {
    //     $.post("/api/subscribe", $("#subscribe-form").serializeArray(), function(data){
    //         try {
    //             var resp = JSON.parse(data);

    //             if (resp.ok) {
    //                 console.log("Good");
    //             } else {
    //                 console.log("Bad");
    //             }
    //         } catch (ex) {
    //             console.log(ex);
    //         }
    //     })
        
    //     return false;
    // }
</script>