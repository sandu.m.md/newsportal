<!-- Footer -->
<div class="container-fluid shadow shadow-lg p-3 mt-5">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">
                <i class="far fa-smile text-secondary btn-text-light"></i><br>
            </h4>
            <h6 class="font-weight-light text-center text-secondary">
                Acest site este în curs de dezvoltare.<br>
                Ne cerem scuze de problemele care pot apărea.
            </h6>
            <?php if(!App::$user->isLoggedIn()) { ?>
                <h6 class="font-weight-light text-center">    
                    <a href="/user/login" class="btn btn-sm btn-light">Autentificare</a>
                    <span> sau </span>
                    <a href="/user/register" class="btn btn-sm btn-light">Înregistrare</a>
                </h6>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h6 class="font-weight-light text-secondary text-center">Copyright &copy; <?php echo date("Y");?></h6>
        </div>
    </div>
</div>