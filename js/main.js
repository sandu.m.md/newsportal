function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
    //   alert("Welcome again " + user);
    } else {
      user = prompt("Please enter your name:", "");
      if (user != "" && user != null) {
        setCookie("username", user, 365);
      }
    }
}

function notif(text, type = "success") {
    html = '<div class="alert alert-'+type+' alert-dismissible position-absolute" style="top:50px;right:10px;z-indez:99">'
        + '<button type="button" class="close" data-dismiss="alert">&times;</button>'
        + text
        + '</div>';

    if ($("#notif").length) {
        $("body").append(html);
    } else {
        $("body").append('<div id="notif">'+html+'</div>');
    }


    setTimeout(function(){
        $("#notif").hide();
    }, 4000);
}

function changeStatus(target, id)
{
    var url = "/posts/enable/";
    if ($(target).attr("checked")) {
        url = "/posts/disable/";
    }
    if (id != 0)
    $.post(url+id, {}, function(data) {
        try {
            var resp = JSON.parse(data);

            if (resp.ok) {
                if (/enable.*/.test(url)) {
                    $(target).attr("checked", true);    
                } else {
                    $(target).removeAttr("checked");
                }
            } else {
                if (/enable.*/.test(url)) {
                    $(target).removeAttr("checked");   
                } else {
                    $(target).attr("checked", true);
                }
            }
        } catch(ex) {
            console.log(ex.message);
            console.log(data);
        }
    });
}